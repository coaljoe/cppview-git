#!/usr/bin/python
#
# This file is in the public domain and comes with no warranty in any form
#
import sys
from PIL import Image
from math import sqrt


def calculateNormalmap(heightmap):
    print heightmap.mode
    #assert(heightmap.mode == 'I') # FIXME
    width, height = heightmap.size

    normalY2 = (2.0 / width) * (2.0 / height)
    scale = 1.0 / 65535.0

    hmData = heightmap.load()

    combined = Image.new('RGBA', heightmap.size)
    cData = combined.load()

    # a "bit" slow...
    for y in xrange(height):
        for x in xrange(width):
            center = hmData[x, y]

            if y > 0:
                top = hmData[x, y - 1]
            else:
                top = center

            if y < height - 1:
                bottom = hmData[x, y + 1]
            else:
                bottom = center

            if x > 0:
                left = hmData[x - 1, y]
            else:
                left = center

            if x < width - 1:
                right = hmData[x + 1, y]
            else:
                right = center

            nx = (left - right) * scale
            nz = (bottom - top) * scale

            l = sqrt(nx ** 2 + normalY2 + nz ** 2)

            nx /= l
            nz /= l


            # write heightmap to red (high) and green (low) channel
            # write normalmap to blue (x) and alpha (z) channel
            r = (center >> 8) & 0xFF
            g = center & 0xFF
            b = int((nx + 1.0) * 255.0 * 0.5)
            a = int((nz + 1.0) * 255.0 * 0.5)

            cData[x, y] = (r, g, b, a)

    return combined




def main():
    if len(sys.argv) != 3:
        print '%s heightmap output' % sys.argv[0]
        sys.exit(1)

    heightmap = Image.open(sys.argv[1])

    combined = calculateNormalmap(heightmap)
    combined.save(sys.argv[2])



if __name__ == '__main__':
    main()


