#ifndef GLOBALS_H
#define GLOBALS_H
#include <Python.h>
#include <Ogre.h>
#include "field.h"

namespace globals
{

extern Ogre::SceneManager *sm;
extern Ogre::Light *light1;
extern Ogre::Camera *cam;
extern float dt;

extern Field *field;
extern PyObject *game_obj;

}

#endif
