#ifndef AUDIOOBJ_H
#define AUDIOOBJ_H
#include <Python.h>
#include <Ogre.h>
#include <OgreOggSound.h>

using namespace Ogre;

extern OgreOggSound::OgreOggSoundManager *soundManager;

void init_sound();
void playSound();

class AudioObj
{
public:
    void spawn() {};

    // sig
    void onUnitFire(PyObject *args);

    AudioObj();
    ~AudioObj();
};

#endif
