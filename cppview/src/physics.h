#ifndef PHYSICS_H
#define PHYSICS_H
#include "Ogre.h"
#include "btBulletDynamicsCommon.h"
#include "LinearMath/btAlignedObjectArray.h"
#include "obj.h"

namespace physics
{

extern btAlignedObjectArray<btCollisionShape*> m_collisionShapes;
extern btBroadphaseInterface*  m_broadphase;
extern btCollisionDispatcher*  m_dispatcher;
extern btConstraintSolver*     m_solver;
extern btDefaultCollisionConfiguration* m_collisionConfiguration;
extern btDynamicsWorld*        m_dynamicsWorld;
extern bool debug;

void init();
void deinit();
void update(float dt);
void debug_init(SceneNode *node);
void addFloor();

class Hinge
{
protected:
    btHingeConstraint* hinge;
public:
    Hinge(Obj *ob1, Obj *ob2, Quaternion basisA, Vector3 originA,
                              Quaternion basisB, Vector3 originB);
    void rotateAngular(float vel, float impulse);
    void rotateToAngle(float angle);
    void enableMotor(bool val);
    void lock();
    float getHingeAngle();
};

} // ::physics

#endif
