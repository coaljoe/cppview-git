#ifndef FX_H
#define FX_H
#include <Python.h>
#include <Ogre.h>

using namespace Ogre;

class FX
{
public:
    static FX* Instance();
    void addExplosion(Vector3 pos, float angle=0);
    void addHit(Vector3 pos);
    void addSmoke(Vector3 pos);

private:
    FX(){}; 
    FX(FX const&){};            
    FX& operator=(FX const&){}; 
    static FX* m_pInstance;
};

#endif
