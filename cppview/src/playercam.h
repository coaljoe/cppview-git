#ifndef PLAYERCAM_H
#define PLAYERCAM_H
#include <Ogre.h>
#include <OIS.h>
#include <SdkCameraMan.h>
#include "globals.h"

using namespace Ogre;

class PlayerCam
{
    bool mScrollUp, mScrollDown,
         mScrollLeft, mScrollRight;
    Real mYaw, mPitch, mViewZoom, mZoomPitch;
    bool mRMBDown;
    SceneNode *mLowCamNode, *mMidCamNode, *mHighCamNode;
    Animation *mCamAnim;
    AnimationState *mCamAnimState;
    NodeAnimationTrack *mCamTrack;
public:
    PlayerCam();
    //void setAutoTracking(Obj *obj);

    Camera* mCamera;
    SceneNode *mCameraNode;
    OgreBites::SdkCameraMan* mCameraMan;

    virtual void mouseMoved(const OIS::MouseEvent& evt);
    virtual void mousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id);
    virtual void mouseReleased(const OIS::MouseEvent& evt, OIS::MouseButtonID id);

    void setDefaultCamera();
    void setLowCam();
    void setMidCam();
    void setHighCam();
    void zoomStep(int step);
    void update(float dt);
};

#endif
