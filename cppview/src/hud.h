#ifndef HUD_H
#define HUD_H
#include <Python.h>
#include <Ogre.h>
#include <OIS.h>
#include "views/unitview.h"

using namespace Ogre;

class SelectionBox;

class HUD
{
private:
    void beginSelection();
    void updateSelection();
    bool performSelection();
    void endSelection();
protected:
    PlaneBoundedVolumeListSceneQuery* mVolQuery;
    std::list<Ogre::MovableObject*> mSelItems;
    SelectionBox* mSelBox;
    Vector2 mSelStart, mSelStop;
    float mMouseX, mMouseY;
    float mSelX, mSelY, mSelW, mSelH;
    int m_state;
    struct States {enum { idle, selection };};
public:
    HUD(void *app);
    ~HUD();

    PyObject *mObj;
    void update(float dt);

    void addUnitSelection(UnitView *uv);
    void removeUnitSelection(UnitView *uv);
    void unselectAll();

    // events
    void onAddSelection(PyObject *obj);
    void onRemoveSelection(PyObject *obj);

    void mousePressed(const OIS::MouseEvent& evt, const OIS::MouseButtonID id);
    void mouseReleased(const OIS::MouseEvent& evt, const OIS::MouseButtonID id);
    void mouseMoved(const OIS::MouseEvent& evt);

};

class SelectionBox : public Ogre::ManualObject
{
public :
    SelectionBox(const Ogre::String& name);
    ~SelectionBox(void);

    void setCorners(float left, float top, float right, float bottom);
    void setCorners(const Ogre::Vector2& topLeft, const Ogre::Vector2& topRight);
};


#endif
