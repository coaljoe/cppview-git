#ifndef VIEW_H
#define VIEW_H
#include <Python.h>
#include <Ogre.h>
#include "audioobj.h"

using namespace Ogre;

// game's Entity View base class
//
class EntityView
{
protected:
    SceneNode* mPivot;
public:
    virtual ~EntityView() {};

    PyObject *mObj;
    bool spawned;
    Vector3 m_pos;
    Quaternion m_ori;
    String m_name;

    // Graphics
    SceneNode* getSceneNode() { return mPivot; }

    void set_model(PyObject *o) { mObj = o; }

    void spawn();
    void update(float dt);

    // Audio
    AudioObj *auObj;
};

EntityView* get_view_by_sn(SceneNode *node);
EntityView* get_view_by_movable(MovableObject *mov);
EntityView* get_view_by_pyobj(PyObject *obj);
EntityView* get_view_by_name(char *onj_name);
bool isUnitView(EntityView *v);
bool isBuildingView(EntityView *v);

#endif
