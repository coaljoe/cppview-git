#include <Python.h>
#include <Ogre.h>
#include "app.h"
//#include "physics.h"
#include "field.h"
#include "event.h"
#include "globals.h"
#include "audioobj.h"

using namespace std;

App::App()
    : mRoot(0),
    mCamera(0),
    mSceneMgr(0),
    mWindow(0),
    mResourcesCfg(Ogre::StringUtil::BLANK),
    mPluginsCfg(Ogre::StringUtil::BLANK),
    mCameraMan(0),
    mShutDown(false),
    mInjectCameraMouse(false),
    mOptAnisotropy(false),
    mInputManager(0),
    mMouse(0),
    mKeyboard(0),
    mGui(0),
    mHud(0),
    mCurrentObject(0),
    mObjectAtCursor(0)
{
    //physics::init();
}

// static
std::vector<UnitView*> App::unitviews;
std::map<std::string, UnitView*> App::unitviews_map;
std::vector<BuildingView*> App::buildingviews;
std::map<std::string, BuildingView*> App::buildingviews_map;
std::vector<EntityView*> App::views;
std::map<std::string, EntityView*> App::views_map;

App* App::m_pInstance = NULL;
App* App::Instance()
{
    if (!m_pInstance) // singleton
        m_pInstance = new App;

    return m_pInstance;
}

App::~App()
{
    //physics::deinit();
    if(mCameraMan) delete mCameraMan;
    if(mHud) delete mHud;
    if(globals::field) delete globals::field;

    CEGUI::OgreRenderer::destroySystem();
    // OIS
    mInputManager->destroyInputObject(mKeyboard); mKeyboard = 0;
    mInputManager->destroyInputObject(mMouse); mMouse = 0;
    OIS::InputManager::destroyInputSystem(mInputManager); mInputManager = 0;

    delete mRoot;
}


void App::buildScene(void)
{
    // light
    //Vector3 lightdir(0.85, -0.3, 0.75);
    Vector3 lightdir(-0.5, -0.8, -0.2);
    lightdir.normalise();

    mLight1 = mSceneMgr->createLight("Light1");
    mLight1->setType(Light::LT_DIRECTIONAL);
    //mLight1->setPosition(Vector3(150, 300, 150));
    mLight1->setDirection(lightdir);
    mLight1->setDiffuseColour(.87, .93, .93);
    mLight1->setSpecularColour(.5, .5, .3);
    mLight1->setCastShadows(true);
    globals::light1 = mLight1; // XXX fixme

    Light* l2 = mSceneMgr->createLight("DirectionalLight");
    l2->setType(Light::LT_DIRECTIONAL);
    //l2->setDirection(Vector3(0, -.5, -8));
    l2->setDirection(Vector3(0, -1, -1));
    //l2->setDiffuseColour(.45, .62, .8);
    l2->setDiffuseColour(.3, .4, .4);
    l2->setCastShadows(false);

    /*
    Light* l2 = mSceneMgr->createLight("DirectionalLight");
    l2->setType(Light::LT_DIRECTIONAL);
    l2->setDirection(Vector3(0, -1, -1));
    l2->setDiffuseColour(.6, .6, .6);
    l2->setCastShadows(false);
    */

    // Misc
    mSceneMgr->setAmbientLight(ColourValue(.5, .5, .5));

    // Scene
    globals::field = new Field();
    globals::field->spawn();

    //physics::addFloor();

    globals::field->test();

}

void App::setDayScene()
{
    // sky
    //mSceneMgr->setSkyPlane(true, Plane(Vector3::NEGATIVE_UNIT_Y, -200),
    //        "Examples/SpaceSkyPlane", 200, 1, true, 0.1, 10, 10);
    //mSceneMgr->setSkyBox(true, "Examples/EarlyMorningSkyBox");
    //mSceneMgr->setSkyBox(true, "Examples/CloudyNoonSkyBox");
    //mSceneMgr->setSkyBox(true, "Examples/MorningSkyBox");
    
    //ColourValue fadeColour(0.85, 0.9, 0.97);
    ColourValue fadeColour;
    fadeColour.setAsRGBA(0x0e3866ff);
    //ColourValue fadeColour(0.45, 0.4, 0.47);
    mWindow->getViewport(0)->setBackgroundColour(fadeColour);
    //mSceneMgr->setFog(FOG_LINEAR, fadeColour, 0.0, 1500, 2000);

    mLight1->setDiffuseColour(.93, .93, .99);
    mLight1->setSpecularColour(.5, .5, .55);
    mSceneMgr->setAmbientLight(ColourValue(.8, .8, .8));
}

bool App::gui_quit(const CEGUI::EventArgs &e)
{
    cout << "CLICK" << endl;
    mShutDown = true;
    return true;
}

void App::run() {
    try { setup(); }
    catch(Exception& e) { fprintf(stderr, "An exception has occurred: %s\n", e.what()); }
    mRoot->startRendering(); 
}

bool App::setup()
{
#ifdef _DEBUG
    mResourcesCfg = "resources_d.cfg";
    mPluginsCfg = "plugins_d.cfg";
#else
    mResourcesCfg = "resources.cfg";
    mPluginsCfg = "plugins.cfg";
#endif

    // Enter Ogre
    mRoot = new Root(mPluginsCfg);

    //LogManager::getSingleton().setLogDetail(LoggingLevel::LL_LOW);
    LogManager::getSingleton().getDefaultLog()->setDebugOutputEnabled(false);

    // Load resource paths
    ConfigFile cf;
    cf.load(mResourcesCfg);

    ConfigFile::SectionIterator seci = cf.getSectionIterator();

    String secName, typeName, archName;
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        ConfigFile::SettingsMultiMap *settings = seci.getNext();
        ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
            ResourceGroupManager::getSingleton().addResourceLocation(
                    archName, typeName, secName, true); // recursive
        }
    }

    // Configure
    if(!mRoot->restoreConfig())
        if(!mRoot->showConfigDialog())
            return false;
    mWindow = mRoot->initialise(true);

    // Resources
    ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    cout << "resource groups initialised" << cout;

    LogManager::getSingleton().getDefaultLog()->setDebugOutputEnabled(true);

    // SceneManager
    globals::sm = mRoot->createSceneManager(ST_GENERIC, "SMInstance");
    mSceneMgr = globals::sm;

    // Camera
    mPlayerCam = new PlayerCam();
    mCamera = mPlayerCam->mCamera;
    mCameraMan = mPlayerCam->mCameraMan;

    Viewport* vp = mWindow->addViewport(mCamera);
    mCamera->setAspectRatio(Real(vp->getActualWidth()) / Real(vp->getActualHeight()));

    globals::cam = mCamera;


    // GUI
    mGui = new GUI(this);

    // CEGUI callback
    mGui->quit_btn->subscribeEvent(CEGUI::PushButton::EventClicked,
                CEGUI::Event::Subscriber(&App::gui_quit, this));

    // HUD
    mHud = new HUD(this);

    // FX
    mFX = FX::Instance();

    // FrameListener
    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    //tell OIS about the Ogre window
    mWindow->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
    if(getenv("DEBUG")) {
        pl.insert(std::make_pair(std::string("x11_mouse_grab"), "false"));
        pl.insert(std::make_pair(std::string("x11_mouse_hide"), "false"));
        pl.insert(std::make_pair(std::string("x11_keyboard_grab"), "false"));
    }

    mInputManager = OIS::InputManager::createInputSystem(pl);
    mKeyboard = static_cast<OIS::Keyboard*> \
               (mInputManager->createInputObject(OIS::OISKeyboard, true));
    mMouse = static_cast<OIS::Mouse*> \
            (mInputManager->createInputObject(OIS::OISMouse, true));
    mKeyboard->setEventCallback(this);
    mMouse->setEventCallback(this);

    // fixing cegui centered cursor position issue
    CEGUI::Point mousePos = CEGUI::MouseCursor::getSingleton().getPosition();
    OIS::MouseState &mutableMouseState = const_cast<OIS::MouseState &>(mMouse->getMouseState());
    mutableMouseState.X.abs = mousePos.d_x;
    mutableMouseState.Y.abs = mousePos.d_y;
    
    //Register as a Window listener
    //Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);

    //tell OIS about the window's dimensions
    unsigned int width, height, depth;
    int top, left;
    mWindow->getMetrics(width, height, depth, left, top);
    const OIS::MouseState &ms = mMouse->getMouseState();
    ms.width = scr_width = width;
    ms.height = scr_height = height;

    mRoot->addFrameListener(this);

    mRayScnQuery = mSceneMgr->createRayQuery(Ray());

    // Misc
    //mSceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);
    //mSceneMgr->setShadowTechnique(SHADOWTYPE_TEXTURE_ADDITIVE);
    mSceneMgr->setShadowTechnique(SHADOWTYPE_TEXTURE_MODULATIVE);
    mSceneMgr->setShadowTextureSelfShadow(false);
    //mSceneMgr->setShadowUseInfiniteFarPlane(true);
    //mSceneMgr->setShadowTechnique(SHADOWTYPE_TEXTURE_ADDITIVE_INTEGRATED);
    //MaterialManager::getSingletonPtr()->setDefaultAnisotropy(4);

    //physics::debug_init(mSceneMgr->getRootSceneNode());
    //physics::debug = true;

    // sound
    init_sound();
    mEarNode= mSceneMgr->getRootSceneNode()->createChildSceneNode();
    mEarNode->setPosition(mCamera->getPosition());
    mEarNode->setOrientation(mCamera->getOrientation());
    mEarNode->attachObject(soundManager->getListener());
    
    cout << "setup ok" << cout;
    return true;
}

bool App::step() {
    Ogre::WindowEventUtilities::messagePump();
    bool ret = mRoot->renderOneFrame();
    if(!ret) {
        delete this;
        return false;
    }
    return true;
}

bool App::keyPressed(const OIS::KeyEvent &e)
{
    //CEGUI::System &sys = CEGUI::System::getSingleton();
    //sys.injectKeyDown(e.key);
    //sys.injectChar(e.text);

    if(e.key == OIS::KC_F12) {
        mPlayerCam->setDefaultCamera();
        mCamera->setFOVy(Degree(20));
    }
    else if(e.key == OIS::KC_F11) {
        mPlayerCam->setDefaultCamera();
        mCamera->setProjectionType(PT_ORTHOGRAPHIC);
        mCamera->setOrthoWindowHeight(200);
    }
    else if(e.key == OIS::KC_F10) {
        mPlayerCam->setDefaultCamera();
    }
    else if(e.key == OIS::KC_I) {
        mInjectCameraMouse = !mInjectCameraMouse;

        if(mInjectCameraMouse)
            mCameraMan->setStyle(OgreBites::CS_FREELOOK);
        else
            mCameraMan->setStyle(OgreBites::CS_MANUAL);

        cout << mCamera->getPosition() << endl;
    }
    else if(e.key == OIS::KC_R) {
        mCamera->getPolygonMode() == PM_SOLID ?
            mCamera->setPolygonMode(PM_WIREFRAME) :
            mCamera->setPolygonMode(PM_SOLID);
    }
    else if(e.key == OIS::KC_T) {
        mOptAnisotropy = !mOptAnisotropy;
        MaterialManager *mm = MaterialManager::getSingletonPtr();
        if(mOptAnisotropy) {
            mm->setDefaultTextureFiltering(TFO_ANISOTROPIC);
            mm->setDefaultAnisotropy(16);
        } else {
            mm->setDefaultTextureFiltering(TFO_BILINEAR);
            mm->setDefaultAnisotropy(1);
        }
    }
    else if(e.key == OIS::KC_1) {
        mPlayerCam->setLowCam();
    }
    else if(e.key == OIS::KC_2) {
        mPlayerCam->setHighCam();
    }
        

    mCameraMan->injectKeyDown(e);
    return true;
}

bool App::keyReleased(const OIS::KeyEvent &e)
{
    mCameraMan->injectKeyUp(e);
    return true;
}

bool App::mousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(_convertButton(id));
    mGui->mousePressed();

    if(!mGui->inFocus())
        mHud->mousePressed(evt, id);

    mPlayerCam->mousePressed(evt, id);
    //mCameraMan->injectMouseDown(evt, id);
    return true;
}

bool App::mouseReleased(const OIS::MouseEvent& evt, OIS::MouseButtonID id)
{
    if(!mGui->inFocus()) // must be first
        mHud->mouseReleased(evt, id);
    CEGUI::System::getSingleton().injectMouseButtonUp(_convertButton(id));
    mPlayerCam->mouseReleased(evt, id);
    //mCameraMan->injectMouseUp(evt, id);
    return true;
}

bool App::mouseMoved(const OIS::MouseEvent& evt)
{
    CEGUI::System::getSingleton().injectMouseMove(evt.state.X.rel, evt.state.Y.rel);

    if(mGui->inFocus())
        return true;

    //find the current mouse position
    CEGUI::Point mousePos = CEGUI::MouseCursor::getSingleton().getPosition();

    //cout << evt.state.width << "x" << evt.state.height << endl;
    //cout << mousePos.d_x << " " << mousePos.d_y << endl;

    //create a raycast straight out from the camera at the mouse's location
    mMouseRay = mCamera->getCameraToViewportRay(mousePos.d_x/float(evt.state.width),
                                                mousePos.d_y/float(evt.state.height));

    Plane mPlane(Vector3::UNIT_Y, Vector3(0, globals::field->getHeightAtCell(0, 0), 0)); // terrain plane, fixme

    std::pair<bool, Real> result = mMouseRay.intersects(mPlane);
    if(result.first)
    {
        Vector3 point = mMouseRay.getPoint(result.second);
        globals::field->setPointerPos(point);
        //cout << globals::field->pointer_x << " " << globals::field->pointer_y << endl;
        //cout << " " << globals::field->getHeightAtCell(
        //globals::field->pointer_x, globals::field->pointer_y) << endl;
    }

    if(1)
    {
        SceneNode *oldobj = mObjectAtCursor;
        SceneNode *newobj = NULL;

        mRayScnQuery->setRay(mMouseRay);
        //mRayScnQuery->setSortByDistance(false);

        // Intersect Movable objects
        RaySceneQueryResult& result = mRayScnQuery->execute();
        RaySceneQueryResult::iterator iter = result.begin();
        for(iter; iter != result.end(); iter++)
        {
            if(!iter->movable
                || iter->movable->getName() == ""
                || iter->movable->getName() == "WaterEntity"
                || iter->movable->getName() == "FieldEntity"
                || iter->movable->getName() == "PlayerCam"
                || StringUtil::startsWith(iter->movable->getName(), "ogre/"))
                continue;

            newobj = iter->movable->getParentSceneNode(); // fixme
            break;
        }

        if(newobj)
        {
            mObjectAtCursor = newobj;
            mObjectAtCursor->showBoundingBox(true);
            
            EntityView *v = dynamic_cast<EntityView*>(get_view_by_movable(iter->movable));
            if(!v) {
                cout << "iter->movable: " << iter->movable->getName() << " !!!" << endl;
                exit(-1);
            }

            //PyObject *args = Py_BuildValue("[s]", obj_name.c_str());
            PyObject *args = Py_BuildValue("[O]", v->mObj);
            send_event("ev_object_mouse_over", NULL, args);
            Py_DECREF(args);

        }

        if(oldobj && oldobj != newobj) {
            oldobj->showBoundingBox(false);
            if(!newobj) {
                oldobj = NULL;
            }
        }

        if(!newobj && !oldobj) {
            mGui->setCursor("MouseArrow");
            mObjectAtCursor = NULL;
        }

    }

    mHud->mouseMoved(evt);
    mPlayerCam->mouseMoved(evt);

    if(mInjectCameraMouse)
        mCameraMan->injectMouseMove(evt);

    return true;
}

bool App::frameRenderingQueued(const FrameEvent& evt)
{
    if(mWindow->isClosed())
        return false;
 
    if(mShutDown)
        return false;

    if(mKeyboard->isKeyDown(OIS::KC_ESCAPE))
        return false;
 
    //Need to capture/update each device
    mKeyboard->capture();
    mMouse->capture();

    float dt = evt.timeSinceLastFrame;
    globals::dt = dt;
    mCameraMan->frameRenderingQueued(evt);
    //physics::update(evt.timeSinceLastFrame);
    this->update(dt);
    mPlayerCam->update(dt);
    mHud->update(dt);
    mGui->update(dt);

    mEarNode->setPosition(mCamera->getPosition());
    mEarNode->setOrientation(mCamera->getOrientation());
    soundManager->update(dt);

    return true;
}

bool App::update(float dt)
{
    // update unit views
    for(int i=0; i < App::unitviews.size(); i++)
    {
        UnitView *uv = App::unitviews[i];
        uv->update(dt);
    }

    // update building views
    for(int i=0; i < App::buildingviews.size(); i++)
    {
        BuildingView *bv = App::buildingviews[i];
        bv->update(dt);
    }

    // update field
    globals::field->update(dt);

    return true;
}
