#ifndef PHYSOBJ_H
#define PHYSOBJ_H
#include "OgreVector3.h"
#include "btBulletDynamicsCommon.h"

using namespace Ogre;

enum { COLSHAPE_BOX, COLSHAPE_SPHERE, COLSHAPE_CYLINDER, COLSHAPE_CONVEX, COLSHAPE_TRIMESH };

class PhysObj
{
protected:
    btScalar mass;
    btScalar friction;
    btVector3 inertia;
public:
    int shapeType;
    btRigidBody* body;
    btCollisionShape* shape;
    void setMass(float _mass) { mass = _mass; }
    void setFriction(float val) { friction = val; }
    void setShapeType(int type_id) { shapeType = type_id; }
    void setLinearVelocity(Vector3 v);
    void rotateToAngle(float angle);
    void translate(Vector3 v);
    void spawn(void *_node, Entity *en);

    PhysObj();
};

#endif
