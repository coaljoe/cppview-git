#ifndef TANKVIEW_H
#define TANKVIEW_H
#include <Python.h>
#include <Ogre.h>
#include <OgreOggSound.h>
#include "unitview.h"

using namespace Ogre;

class TankView : public UnitView
{
private:
    float m_turretAngle;
    SceneNode *mTurretSN;
public:
    TankView();

    virtual void spawn();
    virtual void update(float dt);

    void onUnitFire(PyObject *args);
    void onUnitDestroy(PyObject *args);

    OgreOggSound::OgreOggISound *snd_fire;
};

#endif
