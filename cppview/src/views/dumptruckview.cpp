#include <Python.h>
#include <Ogre.h>
#include "unitview.h"
#include "dumptruckview.h"
#include "globals.h"

using namespace std;
using namespace Ogre;

DumpTruckView::DumpTruckView()
{
    m_dumpBodyAngle = 0;
}

void DumpTruckView::spawn()
{
    UnitView::spawn();

    mDumpBodySN = globals::sm->getSceneNode(m_name + "_dump_body");
}

void DumpTruckView::update(float dt)
{
    UnitView::update(dt);

    //cout << "STATE " << m_state << endl;

    // fetch model variables
    PyObject *_slot = PyObject_GetAttrString(mObj, "dumpBodyAngle");
    m_dumpBodyAngle = PyFloat_AsDouble(_slot);
    Py_DECREF(_slot);

    // update states

    if(m_state == States::unloading) {
        Degree dir = Degree(m_dumpBodyAngle);
        float ar = -dir.valueRadians();

        Vector3 dirVec = Vector3(-cos(ar), sin(ar), 0);
        mDumpBodySN->setDirection(dirVec, Node::TS_WORLD, Vector3(-1, 0, 0));

        //cout << "UNLOADING" << endl;
    }
}
