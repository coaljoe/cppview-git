#include <Python.h>
#include <sip.h>
#include <Ogre.h>
#include <OgreFileSystem.h>
#include "DotSceneLoader.h"
#include "buildingview.h"
#include "globals.h"

using namespace std;
using namespace Ogre;

BuildingView::BuildingView()
{
    m_pos = Vector3(0, 0, 0);
    m_ori = Quaternion(1, 0, 0, 0); //Quaternion::ZERO);
    m_name = StringUtil::BLANK;
    spawned = false;
}

void BuildingView::spawn()
{
    if(spawned) {
        cout << "Already spawned: " << this << " " << endl;
        exit(-1);
    }

    // name
    PyObject *name_slot = PyObject_GetAttrString(mObj, "name");
    m_name = PyString_AsString(name_slot);
    Py_DECREF(name_slot);

    /* update m_pos.y to place on field */
    float x, y;
    PyObject *x_slot = PyObject_GetAttrString(mObj, "x");
    PyObject *y_slot = PyObject_GetAttrString(mObj, "y");
    x = PyFloat_AsDouble(x_slot);
    y = PyFloat_AsDouble(y_slot);
    Py_DECREF(x_slot);
    Py_DECREF(y_slot);

    // spawn at right position XXX fixme
    PyObject *pos_slot = PyObject_GetAttrString(mObj, "pos");
    Vector3 v = *((Vector3 *)((sipSimpleWrapper *)pos_slot)->data);
    m_pos = Vector3(v.x, globals::field->getHeightAtCell(x, y), v.z);
    //PyObject_Print(pos_slot, stderr, 0);
    Py_DECREF(pos_slot);

    // gfx
    SceneManager *sm = globals::sm;
    mPivot = sm->getRootSceneNode()->createChildSceneNode(m_name);
    mPivot->rotate(m_ori);
    mPivot->setPosition(m_pos);

    PyObject *tmp = PyObject_GetAttrString(mObj, "typename");
    char *m_typename = PyString_AsString(tmp);
    Py_DECREF(tmp);

    cout << mObj << " " << m_typename << endl;
    char floc[80];
    if(strcmp(m_typename, "factory") != 0) {
        sprintf(floc, "buildings/%s/scene.xml", m_typename);
    }
    else {
        PyObject *player_ = PyObject_GetAttrString(mObj, "player");
        PyObject *camp_ = PyObject_GetAttrString(player_, "camp");
        char *camp = PyString_AsString(camp_);
        Py_DECREF(player_);
        Py_DECREF(camp_);

        sprintf(floc, "buildings/%s_%s/scene.xml", m_typename, camp);
    }

    printf("Loading scene '%s' from '%s'...\n", m_typename, floc);

    // load .material scripts
    /*
    FileSystemArchive *fsa = new FileSystemArchive("data/buildings/" + string(m_typename), "FileSystem");
    StringVectorPtr sv = fsa->find("*.material", false, false);
    for(StringVector::iterator it = sv->begin(); it != sv->end(); ++it) {
        cout << "YAY" << *it << endl;
        // addResourceLocation
        //exit(-1);
    }
    */

    DotSceneLoader* loader = new DotSceneLoader();
    loader->parseDotScene(floc, "General", sm, mPivot, m_name + "_");

    cout << "Spawned at " << mPivot->getPosition() << " " << mPivot->getOrientation() << endl;
    spawned = true;
}

void BuildingView::update(float dt)
{
}
