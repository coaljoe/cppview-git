#ifndef BUILDINGVIEW_H
#define BUILDINGVIEW_H
#include <Python.h>
#include <Ogre.h>
#include "view.h"

using namespace Ogre;

class BuildingView : public EntityView
{
public:
    BuildingView();
    void spawn();
    void update(float dt);
};

#endif
