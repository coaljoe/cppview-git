#include <Python.h>
#include <sip.h>
#include <Ogre.h>
#include "DotSceneLoader.h"
#include "unitview.h"
#include "fx.h"
#include "globals.h"

using namespace std;
using namespace Ogre;

UnitView::UnitView()
{
    m_pos = Vector3(0, 0, 0);
    m_dir = Degree(0);
    m_name = StringUtil::BLANK;
    m_state = 0;
    spawned = false;

    // bind handlers to signal slots
    sig_onUnitDestroy.connect(boost::bind(&UnitView::onUnitDestroy, this, _1));
    sig_onUnitHit.connect(boost::bind(&UnitView::onUnitHit, this, _1));
    sig_onUnitStateSwitch.connect(boost::bind(&UnitView::onUnitStateSwitch, this, _1));
}

UnitView::~UnitView()
{
}

void UnitView::onSetDir()
{
    mPivot->setOrientation(mPivot->getInitialOrientation());
    mPivot->yaw(Radian(Degree(m_dir)), Node::TS_WORLD);
}

void UnitView::onUnitDestroy(PyObject *args)
{
    FX::Instance()->addSmoke(this->getSceneNode()->getPosition());

    // play default explosion sound
    snd_destroy->play();
}

void UnitView::onUnitHit(PyObject *args)
{
    // add hit particle
    FX::Instance()->addHit(this->getSceneNode()->getPosition());

    // play default hit sound
    snd_hit->play();
}

void UnitView::onUnitStateSwitch(PyObject *args)
{
    PyObject *state_ = PyObject_GetAttrString(mObj, "state");
    int state_n = PyInt_AsLong(state_);
    Py_DECREF(state_);

    cout << "ZZ state:" << state_n << endl;

    if(state_n == 0) { // idle
        snd_moving->stop();
    }
    else if (state_n == 1) { // moving
        snd_moving->play();
    }
}

void UnitView::spawn()
{
    if(spawned) {
        cout << "Already spawned: " << this << " " << endl;
        exit(-1);
    }

    // name
    PyObject *name_slot = PyObject_GetAttrString(mObj, "name");
    m_name = PyString_AsString(name_slot);
    Py_DECREF(name_slot);

    /* update m_pos.y to place on field */
    float x, y;
    PyObject *x_slot = PyObject_GetAttrString(mObj, "x");
    PyObject *y_slot = PyObject_GetAttrString(mObj, "y");
    x = PyFloat_AsDouble(x_slot);
    y = PyFloat_AsDouble(y_slot);
    Py_DECREF(x_slot);
    Py_DECREF(y_slot);

    // spawn at right position XXX fixme
    PyObject *pos_slot = PyObject_GetAttrString(mObj, "pos");
    Vector3 v = *((Vector3 *)((sipSimpleWrapper *)pos_slot)->data);
    m_pos = Vector3(v.x, globals::field->getHeightAtCell(x, y), v.z);
    //PyObject_Print(pos_slot, stderr, 0);
    Py_DECREF(pos_slot);


    // gfx
    SceneManager *sm = globals::sm;
    mPivot = sm->getRootSceneNode()->createChildSceneNode(m_name);
    mPivot->setPosition(m_pos);

    PyObject *tmp = PyObject_GetAttrString(mObj, "typename");
    char *m_typename = PyString_AsString(tmp);
    Py_DECREF(tmp);

    cout << mObj << " " << m_typename << endl;
    char floc[80];
    if(strcmp(m_typename, "lighttank") != 0) {
        sprintf(floc, "units/%s/scene.xml", m_typename);
    }
    else {
        PyObject *player_ = PyObject_GetAttrString(mObj, "player");
        PyObject *camp_ = PyObject_GetAttrString(player_, "camp");
        char *camp = PyString_AsString(camp_);
        Py_DECREF(player_);
        Py_DECREF(camp_);

        sprintf(floc, "units/%s_%s/scene.xml", m_typename, camp);
    }

    printf("Loading scene '%s' from '%s'...\n", m_typename, floc);

    DotSceneLoader* loader = new DotSceneLoader();
    loader->parseDotScene(floc, "General", sm, mPivot, m_name + "_");

    cout << "Spawned at " << mPivot->getPosition() << " " << mPivot->getOrientation() << endl;
    spawned = true;

    // audio
    OgreOggSound::OgreOggISound *snd = 0;
    snd = soundManager->createSound(m_name + "/snd_moving", "moving.ogg", false, true, true);
    snd->setRolloffFactor(30.f);
    snd->setReferenceDistance(100.f);
    mPivot->attachObject(snd);
    snd_moving = snd;

    snd = 0;
    snd = soundManager->createSound(m_name + "/snd_hit", "hit.ogg", false, false, true);
    snd->setRolloffFactor(2.f);
    snd->setReferenceDistance(50.f);
    mPivot->attachObject(snd);
    snd_hit = snd;

    snd = 0;
    snd = soundManager->createSound(m_name + "/snd_destroy", "explosion.ogg", false, false, true);
    snd->setRolloffFactor(2.f);
    snd->setReferenceDistance(50.f);
    mPivot->attachObject(snd);
    snd_destroy = snd;
}

void UnitView::update(float dt)
{
    PyObject *dir_slot = PyObject_GetAttrString(mObj, "dir");
    m_dir = *((Degree *)((sipSimpleWrapper *)dir_slot)->data);
    Py_DECREF(dir_slot);

    /* check events */
    PyObject *ev_onSetDir = PyObject_GetAttrString(mObj, "ev_onSetDir");
    if(PyObject_IsTrue(ev_onSetDir))
    {
        this->onSetDir();
        PyObject_SetAttrString(mObj, "ev_onSetDir", Py_False); 
    }
    Py_DECREF(ev_onSetDir);


    float curY = m_pos.y;

    PyObject *pos_slot = PyObject_GetAttrString(mObj, "pos");
    Vector3 v = *((Vector3 *)((sipSimpleWrapper *)pos_slot)->data);
    m_pos = Vector3(v.x, curY, v.z);
    //PyObject_Print(pos_slot, stderr, 0);
    Py_DECREF(pos_slot);

    //cout << m_pos << endl;

    mPivot->setPosition(m_pos.x, curY, -m_pos.z);

    // states
    PyObject *_slot = PyObject_GetAttrString(mObj, "state");
    m_state = PyInt_AsLong(_slot);
    Py_DECREF(_slot);
}
