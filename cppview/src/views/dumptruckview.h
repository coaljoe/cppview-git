#ifndef DUMPTRUCKVIEW_H
#define DUMPTRUCKVIEW_H
#include <Python.h>
#include <Ogre.h>
#include "unitview.h"

using namespace Ogre;

class DumpTruckView : public UnitView
{
private:
    float m_dumpBodyAngle;
    SceneNode *mDumpBodySN;
public:
    DumpTruckView();

    virtual void spawn();
    virtual void update(float dt);
};

#endif
