#include <Python.h>
#include <Ogre.h>
#include "unitview.h"
#include "tankview.h"
#include "fx.h"
#include "event.h"
#include "globals.h"

using namespace std;
using namespace Ogre;

TankView::TankView() :
    m_turretAngle(0)
{
    // connect in runtime via boost::bind
    sig_onUnitFire.connect(boost::bind(&TankView::onUnitFire, this, _1));
    sig_onUnitDestroy.connect(boost::bind(&TankView::onUnitDestroy, this, _1));
    sig_onUnitFire.connect(&audio_onUnitFire);
    sig_onUnitFire.connect(boost::bind(&AudioObj::onUnitFire, dynamic_cast<AudioObj*>(this), _1));
    //sig_onUnitDestroy.connect(&playSound, "tank_destroy");
    //sig_onUnitDestroy.connect(&playSound);
}

void TankView::onUnitFire(PyObject *args)
{
    Vector3 flame_point_offset = Vector3(5, 2, 0);
    Vector3 v2 = Quaternion(Degree(m_turretAngle) + Degree(m_dir), Vector3::UNIT_Y) * flame_point_offset;

    FX::Instance()->addExplosion(this->getSceneNode()->getPosition() + v2);

    cout << "XXX TANKVIEW FIRE" << endl;

    snd_fire->stop();
    snd_fire->play();
}

void TankView::onUnitDestroy(PyObject *args)
{
    cout << "TankView::onUnitDestroy" << endl;
}

void TankView::spawn()
{
    UnitView::spawn();

    mTurretSN = globals::sm->getSceneNode(m_name + "_turret");
    //mTurretSN->setFixedYawAxis(true, Vector3::UNIT_Y);
    mTurretSN->setInheritOrientation(false);

    // audio
    OgreOggSound::OgreOggISound *snd = 0;
    snd = soundManager->createSound(m_name + "/snd_fire", "tank_fire.ogg", false, false, true);
    snd->setRolloffFactor(2.f);
    snd->setReferenceDistance(50.f);
    mPivot->attachObject(snd);
    snd_fire = snd;
}

void TankView::update(float dt)
{
    UnitView::update(dt);

    // fetch model variables
    PyObject *_slot = PyObject_GetAttrString(mObj, "turretAngle");
    m_turretAngle = PyFloat_AsDouble(_slot);
    Py_DECREF(_slot);

    mTurretSN->setOrientation(mTurretSN->getInitialOrientation());
    mTurretSN->yaw(Radian(m_dir + Degree(m_turretAngle)), Node::TS_WORLD);
}
