#ifndef UNITVIEW_H
#define UNITVIEW_H
#include <Python.h>
#include <Ogre.h>
#include <boost/signals2/signal.hpp>
#include "event.h"
#include "view.h"

using namespace Ogre;

namespace States {
enum { idle, step, moving, loading, unloading };
};

class UnitView : public EntityView
{
protected:
    Degree m_dir;
    int m_state;

public:
    UnitView();
    virtual ~UnitView();

    void onSetDir();
    virtual void spawn();
    virtual void update(float dt);

    virtual void onUnitFire(PyObject *args) {}; // implementation
    void onUnitDestroy(PyObject *args);
    void onUnitHit(PyObject *args);
    void onUnitStateSwitch(PyObject *args);

    SIG sig_onUnitFire;
    SIG sig_onUnitDestroy;
    SIG sig_onUnitHit;
    SIG sig_onUnitStateSwitch;

    OgreOggSound::OgreOggISound *snd_moving;
    OgreOggSound::OgreOggISound *snd_hit;
    OgreOggSound::OgreOggISound *snd_destroy;
};

#endif
