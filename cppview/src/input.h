#ifndef INPUT_H
#define INPUT_H
#include <vector>

class KL
{
public:
    //void execute() { std::cout << "KL exec\n"; }
    //virtual void keypress(int kc) { std::cout << "KL keypress " << kc << std::endl; }
    virtual void keypress(int kc) {};
    virtual void keyrelease(int kc) {};
    void reg();
};

extern std::vector<KL*> kls;

#endif
