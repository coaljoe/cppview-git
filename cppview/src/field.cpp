#include <Python.h>
#include <Ogre.h>
#include "field.h"
#include "app.h"
#include "event.h"
#include "globals.h"

using namespace std;
using namespace Ogre;

const int _terrain_size = 257;
const float _world_size = 1024;
const int _terrain_height = 64;
const int _terrain_height_mul = 1;
const int _shadows_div = 2;
const float _cell_size = 8;
const Vector3 _terrain_position = Vector3(_world_size/2, 10 * _terrain_height_mul, -(_world_size/2));
void spawnWater();

Field::Field() :
    pointer_x(0), pointer_y(0), pointerPos(Vector3::ZERO),
    showMarker(false)
{
}

Field::~Field()
{
}

void Field::update(float dt)
{
    if(showMarker) {
        marker->setVisible(true);
        marker->setPosition(pointer_x * _cell_size, pointerPos.y + 2, -pointer_y * _cell_size);
    }
    else {
        marker->setVisible(false);
    }

}


float Field::getHeightAtCell(int x, int y)
{
    // fixme
    return _terrain_position.y;
}

void Field::test()
{
}

void Field::spawn()
{
    // setup marker
    marker = globals::sm->getRootSceneNode()->createChildSceneNode();
    Entity *en = globals::sm->createEntity("field_marker", "ogrehead.mesh");
    marker->attachObject(en);
    marker->setScale(0.2, 0.2, 0.2);
    marker->setVisible(false);

    mFieldNode = globals::sm->getRootSceneNode()->createChildSceneNode("FieldNode");
    Entity *terrEnt = globals::sm->createEntity("FieldEntity", "field/field.mesh");
    terrEnt->setMaterialName("field");
    terrEnt->setCastShadows(false);
    mFieldNode->setScale(500, 500, 500);
    mFieldNode->setPosition(_terrain_position);
    mFieldNode->translate(Vector3(0, -65, 0));
    mFieldNode->attachObject(terrEnt);


    spawnWater();
}

void spawnWater()
{
    Plane plane(Vector3::UNIT_Y, 0);
 
    MeshManager::getSingleton().createPlane("water", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
        plane, _world_size, _world_size, 10, 10, true, 1, 15, 15, Vector3::UNIT_Z);
 
    Entity* entWater = globals::sm->createEntity("WaterEntity", "water");
    SceneNode *nodeWater = globals::sm->getRootSceneNode()->createChildSceneNode();
    nodeWater->attachObject(entWater);
 
    nodeWater->setPosition(_world_size/2, 0, -_world_size/2);
    entWater->setMaterialName("Water1");
    entWater->setCastShadows(false);
    //entWater->setRenderQueueGroup(RENDER_QUEUE_9);
}

void Field::setPointerPos(Vector3 v)
{
    pointerPos = v;
    int old_x = pointer_x;
    int old_y = pointer_y;
    pointer_x = (int)floor((v.x + 0.5) / _cell_size);
    pointer_y = (int)floor((-v.z + 0.5) / _cell_size);

    PyObject_SetAttrString(mObj, "pointer_x", PyInt_FromLong(pointer_x));
    PyObject_SetAttrString(mObj, "pointer_y", PyInt_FromLong(pointer_y));

#if 0
    if((old_x != pointer_x) || (old_y != pointer_y)) {
        PyObject *args = Py_BuildValue("[iiii]", pointer_x, pointer_y, old_x, old_y);
        send_event("ev_field_cell_activate", NULL, args);
        Py_DECREF(args);
    }
#endif
}
