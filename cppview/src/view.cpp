#include <Ogre.h>
#include <OIS.h>
#include "app.h"
#include "view.h"

using namespace std;

// fetch a view by its nodes
EntityView* get_view_by_sn(SceneNode *node)
{
    String obj_name = node->getParentSceneNode()->getName();

    if(!App::views_map.count(obj_name)) {
        cout << "view not found; obj_name: " << obj_name << endl;
        return NULL;
    }

    EntityView *v = App::views_map.find(obj_name)->second;

    return v;
}

EntityView* get_view_by_movable(MovableObject *mov)
{
    return get_view_by_sn(mov->getParentSceneNode());
}

EntityView* get_view_by_pyobj(PyObject *obj)
{
    PyObject *obj_name_slot;
    obj_name_slot = PyObject_GetAttrString(obj, "name"); 
    char *obj_name = PyString_AsString(obj_name_slot);
    Py_DECREF(obj_name_slot);

    EntityView *v = App::views_map.find(obj_name)->second;

    return v;
}

EntityView* get_view_by_name(char *obj_name)
{
    // depr?
    EntityView *v = App::views_map.find(obj_name)->second;

    return v;
}

bool isUnitView(EntityView *v)
{
    return (App::unitviews_map.count(v->m_name)) ? true : false;
}

bool isBuildingView(EntityView *v)
{
    return (App::buildingviews_map.count(v->m_name)) ? true : false;
}
