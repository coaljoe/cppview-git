#include <Python.h>
#include <Ogre.h>
#include "event.h"
#include "app.h"
#include "view.h"
#include "views/unitview.h"
#include "views/dumptruckview.h"
#include "views/tankview.h"
#include "views/buildingview.h"
#include "globals.h"

using namespace std;

int main(int argc, char **argv) {
    cout << "main()\n";

    App *app = App::Instance();
    //app->run();

    app->setup();
    app->buildScene();
    app->setDayScene();

    // run python module
    const char *MODNAME = "src.main";
    PyObject *modMain, *game_obj;

    Py_Initialize();

    modMain = PyImport_Import(PyString_FromString(MODNAME));
    if (!modMain) {
        PyErr_Print();
        fprintf(stderr, "Failed to load \"%s\"\n", MODNAME);
        return 1;
    }

    game_obj = PyObject_GetAttrString(modMain, "game");
    if (!game_obj) {
        if (PyErr_Occurred())
            PyErr_Print();
        fprintf(stderr, "Cannot find function \"%s\"\n", "game");
        return 1;
    }
    globals::game_obj = game_obj;

    PyObject *update_meth;
    update_meth = PyObject_GetAttrString(game_obj, "update");
    PyObject_Print(update_meth, stderr, 0);
    fprintf(stderr, "\n");
    //PyObject *objs = PyObject_GetAttrString(game_obj, "objs");
    //PyObject_Print(objs, stderr, 0);
    //fprintf(stderr, "\n");

    // process events
    //PyObject *modEvent = PyImport_Import(PyString_FromString("src.event"));
    PyObject *modEvent = PyObject_GetAttrString(game_obj, "event");
    _modEvent = modEvent;

    // set field model
    PyObject *field = PyObject_GetAttrString(game_obj, "field");
    globals::field->set_model(field);

    Py_DECREF(field);

    // set hud model fixme
    app->mHud->mObj = PyObject_GetAttrString(game_obj, "hud");


    while(1)
    {
        float dt = globals::dt;

        // python: game.update
        PyObject *arglist, *result;
        arglist = Py_BuildValue("(f)", dt);
        result = PyObject_CallObject(update_meth, arglist);

        if (!result) {
            fprintf(stderr, "\ngame.update call error\n");
            if (PyErr_Occurred())
                PyErr_Print();
            return 1;
        }

        Py_XDECREF(result);
        Py_DECREF(arglist);

        // python: process events
        PyObject *event_queue = PyObject_GetAttrString(modEvent, "event_queue"); // list
        for(int i=0; i < PyList_GET_SIZE(event_queue); i++)
        {
            PyObject *ev, *ev_sender, *ev_name_;
            ev = PyList_GetItem(event_queue, i); // tuple
            ev_sender = PySequence_GetItem(ev, 1);
            ev_name_ = PySequence_GetItem(ev, 0);
            char *ev_name = PyString_AsString(ev_name_);
            Py_XDECREF(ev_name_);

            cout << endl << "EVNAME " << ev_name << endl;
            PyObject_Print(ev_sender, stderr, 0);
            fprintf(stderr, "\n");
        
            if(strcmp(ev_name, "ev_unit_spawn") == 0)
            {
                PyObject *tmp = PyObject_GetAttrString(ev_sender, "typename");
                char *typename_ = PyString_AsString(tmp);
                Py_DECREF(tmp);

                UnitView *uv;

                // create unit view
                if(strcmp(typename_, "tank") == 0 || strcmp(typename_, "lighttank") == 0)
                {
                    uv = new TankView();
                }
                else if(strcmp(typename_, "dump_truck") == 0)
                {
                    uv = new DumpTruckView();
                }
                else
                {
                    cout << "ERR unknown unit typename: " << typename_ << endl;
                    exit(-1);
                }

                uv->set_model(ev_sender);
                uv->spawn();
                App::unitviews.push_back(uv);

            	PyObject *slot_ = PyObject_GetAttrString(ev_sender, "name");
                char *name = PyString_AsString(slot_);
            	Py_DECREF(slot_);
                App::unitviews_map.insert(pair<std::string, UnitView*>(String(name), uv));

                App::views.push_back(dynamic_cast<EntityView*>(uv));
                App::views_map.insert(pair<std::string, EntityView*>(String(name), dynamic_cast<EntityView*>(uv)));
            }
            else if(strcmp(ev_name, "ev_building_spawn") == 0)
            {
                BuildingView *bv;

                bv = new BuildingView();
                bv->set_model(ev_sender);
                bv->spawn();
                App::buildingviews.push_back(bv);

            	PyObject *slot_ = PyObject_GetAttrString(ev_sender, "name");
                char *name = PyString_AsString(slot_);
            	Py_DECREF(slot_);
                App::buildingviews_map.insert(pair<std::string, BuildingView*>(String(name), bv));

                App::views.push_back(dynamic_cast<EntityView*>(bv));
                App::views_map.insert(pair<std::string, EntityView*>(String(name), dynamic_cast<EntityView*>(bv)));
            }
#if 0
            else if(strcmp(ev_name, "ev_unit_select") == 0) // XXX remove: src cannot select units - only hud can
            {
                //PyObject *obj = NULL;
                PyObject *slot_ = PyObject_GetAttrString(ev_sender, "name");
                char *name = PyString_AsString(slot_);
                Py_DECREF(slot_);

                UnitView *uv = App::unitviews_map.find(name)->second;
                app->mHud->addUnitSelection(uv);
            }
#endif
            else if(strcmp(ev_name, "ev_gui_set_cursor") == 0)
            {
                PyObject *_args_tuple = PySequence_GetItem(ev, 2); // args-kw tuple
                PyObject *args = PySequence_GetItem(_args_tuple, 0); // args list
                PyObject *args_name = PySequence_GetItem(args, 0); 
                app->mGui->setCursor(PyString_AsString(args_name));
                Py_DECREF(args_name);
                Py_DECREF(args);
                Py_DECREF(_args_tuple);
            }
            else if(strcmp(ev_name, "ev_hud_add_selection") == 0)
            {
                PyObject *_args_tuple = PySequence_GetItem(ev, 2); // args-kw tuple
                PyObject *args = PySequence_GetItem(_args_tuple, 0); // args list
                PyObject *args_obj = PySequence_GetItem(args, 0); 
                app->mHud->onAddSelection(args_obj);
                Py_DECREF(args_obj);
                Py_DECREF(args);
                Py_DECREF(_args_tuple);
            }
            else if(strcmp(ev_name, "ev_hud_remove_selection") == 0)
            {
                PyObject *_args_tuple = PySequence_GetItem(ev, 2); // args-kw tuple
                PyObject *args = PySequence_GetItem(_args_tuple, 0); // args list
                PyObject *args_obj = PySequence_GetItem(args, 0); 
                app->mHud->onRemoveSelection(args_obj);
                Py_DECREF(args_obj);
                Py_DECREF(args);
                Py_DECREF(_args_tuple);
            }
            else 
            {
                // current type messages/events
                handle_event(ev_name, ev);
            }
            //Py_DECREF(ev);
            //Py_DECREF(ev_sender);
        }
        
        Py_DECREF(event_queue);


        // python: event.clear_queue
        PyObject *clear_queue_ = PyObject_GetAttrString(modEvent, "clear_queue");
        result = PyObject_CallObject(clear_queue_, NULL);

        if (!result) {
            fprintf(stderr, "\nevent.clear_queue call error\n");
            if (PyErr_Occurred())
                PyErr_Print();
            return 1;
        }

        Py_XDECREF(result);
        Py_XDECREF(clear_queue_);

        if(!app->step())
            break;
    }

    Py_DECREF(modEvent);
    Py_XDECREF(update_meth);
    Py_XDECREF(game_obj);
    Py_DECREF(modMain);
    Py_Finalize();

    return 0;
}
