#ifndef WORLD_H
#define WORLD_H
#include "obj.h"

namespace world
{

void add_obj(Obj* o);
void update(float dt);

} //::world

#endif
