#ifndef APP_H
#define APP_H
#include <Ogre.h>
#include <OIS.h>
#include "gui.h"
#include "hud.h"
#include "fx.h"
#include "playercam.h"
#include "view.h"
#include "views/unitview.h"
#include "views/buildingview.h"
#include "views/dumptruckview.h"
#include "views/buildingview.h"

using namespace Ogre;

class App : public FrameListener, public OIS::KeyListener, public OIS::MouseListener
{
private:
    Light *mLight1;
    bool update(float dt);

    static App* m_pInstance;
    App();
    virtual ~App();
public:
    static App* Instance();

    void buildScene();
    void setDayScene();
    bool setup();
    bool step();
    void run();
    bool gui_quit(const CEGUI::EventArgs &e);

    SceneManager* getSM() { return this->mSceneMgr; }
    GUI* mGui;
    HUD* mHud;
    FX* mFX;
    PlayerCam *mPlayerCam;
    RenderWindow* mWindow;

    static std::vector<UnitView*> unitviews;
    static std::map<std::string, UnitView*> unitviews_map;
    static std::vector<BuildingView*> buildingviews;
    static std::map<std::string, BuildingView*> buildingviews_map;
    static std::vector<EntityView*> views; // all views
    static std::map<std::string, EntityView*> views_map;

    SceneNode *mCurrentObject;
    SceneNode *mObjectAtCursor;
    SceneNode *mEarNode;

    int scr_height, scr_width;

protected:
    Root* mRoot;
    Camera* mCamera;
    OgreBites::SdkCameraMan* mCameraMan;
    SceneManager* mSceneMgr;
    String mResourcesCfg;
    String mPluginsCfg;

    OIS::InputManager* mInputManager;
    OIS::Mouse*    mMouse;
    OIS::Keyboard* mKeyboard;
    RaySceneQuery* mRayScnQuery;
    Ray mMouseRay;

    bool mShutDown;
    bool mInjectCameraMouse;
    bool mOptAnisotropy;

    virtual bool frameRenderingQueued(const FrameEvent& evt);
    virtual bool keyPressed(const OIS::KeyEvent &arg);
    virtual bool keyReleased(const OIS::KeyEvent &arg);
    virtual bool mouseMoved(const OIS::MouseEvent &arg);
    virtual bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
    virtual bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
};

#endif
