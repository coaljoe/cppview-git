#include <Python.h>
#include <iostream>
#include <boost/signals2/signal.hpp>
#include "view.h"
#include "views/unitview.h"
#include "event.h"
#include "app.h"

using namespace std;

PyObject *_modEvent;
//std::map <std::string ev_name, void signal<void PyObject *args>> events_map;

// 'args' should be of PyList type
bool send_event(const char *ev_name, PyObject *kw, PyObject *args)
{
    PyObject *ev_obj, *sender, *result;

    ev_obj = PyObject_GetAttrString(_modEvent, "pub");
    sender = PyString_FromString(ev_name);

     // set args to sender/sender+args
    if(args != NULL) {
        PyList_Insert(args, 0, sender);
        args = PyList_AsTuple(args);
        //PyObject_Print(args, stdout, 0);
    }
    else {
        args = Py_BuildValue("(O)", sender);
    }

    result = PyObject_Call(ev_obj, args, kw);

    if(result == NULL)
        if(PyErr_Occurred())
            printf("event '%s' call error\n", ev_name);
            PyErr_Print();

    Py_XDECREF(result);
    Py_DECREF(sender);
    Py_DECREF(ev_obj);

    return true;
}

void handle_event(char *ev_name, PyObject *ev)
{
    PyObject *_args_tuple = PySequence_GetItem(ev, 2); // args-kw tuple
    PyObject *args = PySequence_GetItem(_args_tuple, 0); // args list
    cout << "XXX HANDLE_EVENT " << ev_name << endl;

    if(strcmp(ev_name, "ev_unit_fire") == 0)
    {
        PyObject *args_obj = PySequence_GetItem(args, 0); 

        UnitView *uv = dynamic_cast<UnitView*>(get_view_by_pyobj(args_obj));
        uv->sig_onUnitFire(args); // firing up signal slot

        //boost::signals2::signal *sig = events_map.find(ev_name)->second;
        //sig(args); // fire up the event

        Py_DECREF(args_obj);
    }
    else if(strcmp(ev_name, "ev_unit_destroy") == 0)
    {
        PyObject *args_obj = PySequence_GetItem(args, 0); 

        UnitView *uv = dynamic_cast<UnitView*>(get_view_by_pyobj(args_obj));
        uv->sig_onUnitDestroy(args);

        Py_DECREF(args_obj);
    }
    else if(strcmp(ev_name, "ev_unit_hit") == 0)
    {
        PyObject *args_obj = PySequence_GetItem(args, 0); 

        UnitView *uv = dynamic_cast<UnitView*>(get_view_by_pyobj(args_obj));
        uv->sig_onUnitHit(args);

        Py_DECREF(args_obj);
    }
    else if(strcmp(ev_name, "ev_unit_state_switch") == 0)
    {
        PyObject *args_obj = PySequence_GetItem(args, 0); 

        UnitView *uv = dynamic_cast<UnitView*>(get_view_by_pyobj(args_obj));
        uv->sig_onUnitStateSwitch(args);

        Py_DECREF(args_obj);
    }
    else if(strcmp(ev_name, "ev_gui_show_window") == 0)
    {
        PyObject *args_obj = PySequence_GetItem(args, 0); 

        App::Instance()->mGui->sig_onGuiShowWindow(args);

        Py_DECREF(args_obj);
    }

    Py_DECREF(_args_tuple);
    Py_DECREF(args);
}

void audio_onUnitFire(PyObject *args)
{
    cout << "Audio: onUnitFire" << endl;
}

void init()
{
}
