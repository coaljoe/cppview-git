#ifndef EVENT_H
#define EVENT_H
#include <Python.h>
#include <boost/signals2/signal.hpp>

extern PyObject *_modEvent;
bool send_event(const char *ev_name, PyObject *kw=NULL, PyObject *args=NULL);
void handle_event(char *ev_name, PyObject *ev);
void audio_onUnitFire(PyObject *args);

typedef boost::signals2::signal< void (PyObject *args) > SIG; // basic signal typedef

#endif
