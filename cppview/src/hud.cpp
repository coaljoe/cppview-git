#include <Python.h>
#include <Ogre.h>
#include <OIS.h>
#include "app.h"
#include "event.h"
#include "view.h"
#include "views/unitview.h"
#include "hud.h"
#include "event.h"
#include "globals.h"

using namespace std;
using namespace Ogre;

App *_app;

HUD::HUD(void *app)
    : m_state(States::idle), mObj(0),
      mMouseX(0), mMouseY(0),
      mSelX(0), mSelY(0), mSelW(0), mSelH(0)
{
    _app = (App *)app;
    mSelBox = new SelectionBox("SelectionBox");
    globals::sm->getRootSceneNode()->createChildSceneNode()->attachObject(mSelBox);

    mVolQuery = globals::sm->createPlaneBoundedVolumeQuery(PlaneBoundedVolumeList());

}

HUD::~HUD()
{
    globals::sm->destroyQuery(mVolQuery);
    if(mSelBox) delete mSelBox;
    Py_DECREF(mObj);
}

void HUD::update(float dt)
{
    PyObject_SetAttrString(mObj, "state", PyInt_FromLong(m_state));
}

void HUD::mouseMoved(const OIS::MouseEvent& evt)
{
    if(evt.state.buttonDown(OIS::MB_Left))
    {
        if(m_state == States::idle) {
            beginSelection();
            m_state = States::selection;
        }
        else if(m_state == States::selection) {
            updateSelection();
        }
    }
}

void HUD::mousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id)
{

}

void HUD::mouseReleased(const OIS::MouseEvent& evt, OIS::MouseButtonID id)
{
    if(!id == OIS::MB_Left) {
        send_event("ev_hud_unselect_all");
        return;
    }

    // handle clicks
    if(_app->mObjectAtCursor) {
        // click on object
        EntityView *v = get_view_by_sn(_app->mObjectAtCursor);
        PyObject *args = Py_BuildValue("[O]", v->mObj);
        send_event("ev_object_click", NULL, args);
        Py_DECREF(args);
    }
    else
    {
        // click on field
        if(m_state == States::selection) {
            endSelection();
            m_state = States::idle;
        }
        else {
            send_event("ev_field_click");
            _app->mGui->hideAllWindows();
        }
    }

    // handle states
    if(m_state == States::idle) {
    }
    else if(m_state == States::selection) {
        //m_state = States::idle;
    }
}

void HUD::beginSelection()
{
    CEGUI::MouseCursor* mouse = CEGUI::MouseCursor::getSingletonPtr();
    mSelStart.x = mouse->getPosition().d_x / _app->scr_width;
    mSelStart.y = mouse->getPosition().d_y / _app->scr_height;
    mSelStop = mSelStart;

    mSelBox->clear();
    mSelBox->setVisible(true);

    send_event("ev_hud_begin_selection");
}

void HUD::updateSelection()
{
    // update selection
    CEGUI::MouseCursor* mouse = CEGUI::MouseCursor::getSingletonPtr();
    mSelStop.x = mouse->getPosition().d_x / _app->scr_width;
    mSelStop.y = mouse->getPosition().d_y / _app->scr_height;
    mSelBox->setCorners(mSelStart, mSelStop);
}

void HUD::endSelection()
{
    unselectAll();
    performSelection();

    mSelBox->setVisible(false);

    send_event("ev_hud_end_selection");
}

void HUD::onAddSelection(PyObject *obj)
{
    UnitView *uv = dynamic_cast<UnitView*>(get_view_by_pyobj(obj));
    addUnitSelection(uv);
}

void HUD::addUnitSelection(UnitView *uv)
{
    cout << "HUD::addUnitSelection " << uv->m_name << endl;

    Camera *cam = globals::cam;
    Vector3 pos = uv->m_pos; pos.z = -pos.z; // fix z position
    Real x, y;

    //Entity *en = globals::sm->createEntity(tmpnam(NULL), "ogrehead.mesh");

    ManualObject *manual = globals::sm->createManualObject();

    manual->begin("BaseWhiteNoLighting", RenderOperation::OT_LINE_STRIP);

    manual->position( 0.0,-0.4, 0.0);
    manual->position( 0.8, 0.8, 0.0);
    manual->position(-0.8, 0.8, 0.0);

    manual->index(0);
    manual->index(1);
    manual->index(2);
    manual->index(0);

    manual->end();

    // Use infinite AAB to always stay visible
    AxisAlignedBox aabInf;
    aabInf.setInfinite();
    manual->setBoundingBox(aabInf);

    // Render just before overlays
    manual->setRenderQueueGroup(RENDER_QUEUE_OVERLAY);
    
    manual->setCastShadows(false);

    SceneNode *node;
    node = uv->getSceneNode()->createChildSceneNode(uv->m_name + "_sel");
    node->setPosition(0, 3, -1);
    node->attachObject(manual);
    //node->setScale(0.1, 0.1, 0.1);
}

void HUD::onRemoveSelection(PyObject *obj)
{
    UnitView *uv = dynamic_cast<UnitView*>(get_view_by_pyobj(obj));
    removeUnitSelection(uv);
}

void HUD::removeUnitSelection(UnitView *uv)
{
    cout << "HUD::removeUnitSelection " << uv->m_name << endl;

    try {
        uv->getSceneNode()->removeAndDestroyChild(uv->m_name + "_sel");
    }
    catch(Exception& e) { fprintf(stderr, "An exception has occurred: %s\n", e.what()); }

}


void HUD::unselectAll()
{
    cout << "HUD::unselectAll" << endl;

/*
    PyObject *selectedUnits_obj = PyObject_GetAttrString(mObj, "selectedUnits");
    for(int i=0; i< PyList_GET_SIZE(selectedUnits_obj); i++)
    {
        PyObject *item = PyList_GetItem(selectedUnits_obj, i);

        UnitView *uv = dynamic_cast<UnitView*>(get_view_by_pyobj(item));
        removeUnitSelection(uv);

        Py_XDECREF(item);
    }
*/

    for(std::list<MovableObject*>::iterator it = mSelItems.begin();
        it != mSelItems.end(); it++) {
        (*it)->getParentSceneNode()->showBoundingBox(false);
    }

    mSelItems.clear();
}

void swap(float& x, float& y)
{
    float temp = x;
    x = y;
    y = temp;
}

bool HUD::performSelection()
{
    Vector2 first = mSelStart;
    Vector2 second = mSelStop;

    float left = first.x, right = second.x;
    float top = first.y, bottom = second.y;

    if(left > right) {
        swap(left, right);
    }
    if(top > bottom) {
        swap(top, bottom);
    }

    if((right - left) * (bottom - top) < 0.0001)
        return false;

    Ray topLeft = globals::cam->getCameraToViewportRay(left, top);
    Ray topRight = globals::cam->getCameraToViewportRay(right, top);
    Ray bottomLeft = globals::cam->getCameraToViewportRay(left, bottom);
    Ray bottomRight = globals::cam->getCameraToViewportRay(right, bottom);

    PlaneBoundedVolume vol;

    vol.planes.push_back(Plane(topLeft.getPoint(3), topRight.getPoint(3), bottomRight.getPoint(3)));
    vol.planes.push_back(Plane(topLeft.getOrigin(), topLeft.getPoint(100), topRight.getPoint(100)));
    vol.planes.push_back(Plane(topLeft.getOrigin(), bottomLeft.getPoint(100), topLeft.getPoint(100)));
    vol.planes.push_back(Plane(bottomLeft.getOrigin(), bottomRight.getPoint(100), bottomLeft.getPoint(100)));
    vol.planes.push_back(Plane(topRight.getOrigin(), topRight.getPoint(100), bottomRight.getPoint(100)));

    PlaneBoundedVolumeList volList;
    volList.push_back(vol);
    mVolQuery->setVolumes(volList);

    SceneQueryResult& result = mVolQuery->execute();
    //unselectAll();

    for(SceneQueryResultMovableList::iterator it = result.movables.begin();
       it != result.movables.end(); it++)
    {
        //(*it)->getParentSceneNode()->showBoundingBox(true);
        mSelItems.push_back(*it);
    }

    cout << "processing mSelItems..." << endl;
    for(std::list<MovableObject*>::iterator it = mSelItems.begin();
        it != mSelItems.end(); it++)
    {
        SceneNode *node = (*it)->getParentSceneNode();

        if(node->getName() == "Ogre/SceneRoot") // XXX fixme
            continue;

        //cout << node->getName() << endl;
        String obj_name = node->getParentSceneNode()->getName();

        cout << "Selecting: " << obj_name << endl;

        if(App::unitviews_map.count(obj_name) > 0) {
            cout << "FOUND " << obj_name << endl;

            // controller event push
            UnitView *uv = App::unitviews_map.find(obj_name)->second;
            PyObject *args = Py_BuildValue("[O]", uv->mObj);
            send_event("ev_hud_unit_select", NULL, args);
            Py_DECREF(args);
        }
    }

    return true;
}

/* SelectionBox */

SelectionBox::SelectionBox(const String& name): ManualObject(name)
{
    setRenderQueueGroup(RENDER_QUEUE_OVERLAY);
    setUseIdentityProjection(true);
    setUseIdentityView(true);
    setQueryFlags(0);
    setCastShadows(false);
}

SelectionBox::~SelectionBox()
{
}

//sets the actual corners of the box
void SelectionBox::setCorners(float left, float top, float right, float bottom)
{
    left = left * 2 - 1;
    right = right * 2 - 1;
    top = 1 - top * 2;
    bottom = 1 - bottom * 2;

    clear();
    begin("BaseWhiteNoLighting",RenderOperation::OT_LINE_STRIP);
        position(left, top, -1);
        position(right, top, -1);
        position(right, bottom, -1);
        position(left, bottom, -1);
        position(left, top, -1);
    end();

    setBoundingBox(AxisAlignedBox::BOX_INFINITE);
}

//an overridden function to do the same as before but you can pass vector2s instead
void SelectionBox::setCorners(const Vector2& topLeft, const Vector2& bottomRight)
{
    setCorners(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
}
