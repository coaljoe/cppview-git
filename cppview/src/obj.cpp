#include <iostream>
#include "Ogre.h"
#include "globals.h"
#include "obj.h"

using namespace std;
using namespace Ogre;

Obj::Obj(Vector3 pos)
{
    // Deprecated.
    // default object's state initialization
    m_pos = pos;
    m_ori = Quaternion::IDENTITY;
    phys = PhysObj();

    spawned = false;
}

Obj::Obj()
{
    // default object's state initialization
    m_pos = Vector3::ZERO;
    m_ori = Quaternion::IDENTITY;
    phys = PhysObj();

    m_meshFile = "NOT_SET";
    m_materialName = "NOT_SET";

    spawned = false;
}

void Obj::setMesh(const char *s)
{
    m_meshFile = (char *)s;
}

void Obj::setMaterial(const char *s)
{
    m_materialName = (char *)s;
}

void Obj::rotate(Vector3 axis, Radian angle)
{
    //Node::TransformSpace ts = Node::TS_WORLD;
    //mPivot->rotate(Vector3::UNIT_Z, Degree(90), ts);
    //mPivot->rotate(axis, angle);
    m_ori.FromAngleAxis(angle, axis);
}


void Obj::spawn()
{
    if(spawned) {
        cout << "Already spawned: " << this << " " << endl;
        exit(-1);
    }

    // gfx
    SceneManager *sm = globals::sm;
    mPivot = sm->getRootSceneNode()->createChildSceneNode();

    SceneNode* node = mPivot->createChildSceneNode();
    mPivot->rotate(m_ori);
    mPivot->setPosition(m_pos);

    cout << "Spawned at " << mPivot->getPosition() << " " << mPivot->getOrientation() << endl;
    Entity* en;

    if(strlen(m_meshFile) == 0)
        en = sm->createEntity(tmpnam(NULL), "ogrehead.mesh");
    else 
        en = sm->createEntity(tmpnam(NULL), m_meshFile);
    node->attachObject(en);

    if(strlen(m_materialName) != 0) {
        en->setMaterialName(m_materialName);
    }

    // phys
    phys.spawn(mPivot, en);
    
    spawned = true;
    
    //world::add_obj(this);
}

void Obj::addNode(const char *name, const char *mesh, Vector3 pos, Quaternion ori)
{
    SceneNode* node = mPivot->createChildSceneNode();
    node->rotate(ori);
    node->setPosition(pos);

    Entity* en;
    en = globals::sm->createEntity(name, mesh);
    node->attachObject(en);
}

