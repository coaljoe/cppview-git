#include <Ogre.h>
#include "playercam.h"
#include "globals.h"

using namespace std;
using namespace Ogre;

PlayerCam::PlayerCam() :
    mScrollUp(0), mScrollDown(0),
    mScrollLeft(0), mScrollRight(0),
    mPitch(0), mYaw(0), mViewZoom(40), mZoomPitch(0),
    mRMBDown(false),
    mCamAnimState(NULL)
{
    // Camera
    mCamera = globals::sm->createCamera("PlayerCam"); 

    //mCamera->setPosition(100, 460, 580);
    mCamera->setPosition(65, 350, 450);
    //mCamera->lookAt(Vector3(0, 0, 0));
    this->setDefaultCamera();
    mCamera->setFOVy(Degree(20));

    mCamera->setNearClipDistance(0.1);
    mCamera->setFarClipDistance(50000);
    mCamera->setLodBias(5);

    /*
    mCamera->setProjectionType(PT_ORTHOGRAPHIC);
    mCamera->setOrthoWindowHeight(20);
    */

    mCameraMan = new OgreBites::SdkCameraMan(mCamera);
    mCameraMan->setStyle(OgreBites::CS_MANUAL);

    mCameraNode = globals::sm->getRootSceneNode()->createChildSceneNode("CameraNode");
    mCameraNode->attachObject(mCamera);

    mLowCamNode = mCameraNode->createChildSceneNode("LowCamNode");
    mHighCamNode = mCameraNode->createChildSceneNode("HighCamNode");

    mLowCamNode->translate(Vector3(0, -10, 0));
    mHighCamNode->translate(Vector3(0, 10, 0));

    float duration = 4;
    Animation* mCamAnim = globals::sm->createAnimation("Camera Animation", duration);
    mCamAnim ->setInterpolationMode(Animation::IM_LINEAR);

    NodeAnimationTrack* mCamTrack = mCamAnim->createNodeTrack(0, mCameraNode);
    mCamTrack->setUseShortestRotationPath(true);

    // linear transform keys
    TransformKeyFrame* key;

    // lowcam
    key = mCamTrack->createNodeKeyFrame(0);
    key->setTranslate(Vector3(100, -10, 0));
    //key->setTranslate(mCameraNode->getPosition() - mLowCamNode->getPosition());
    
    // midcam
    //key= mCamTrack->createNodeKeyFrame(1);
    //key->setTranslate(Vector3(0, 0, 0));

    // highcam
    key = mCamTrack->createNodeKeyFrame(2);
    key->setTranslate(Vector3(100, 10, 0));
    //key->setTranslate(mCameraNode->getPosition() - mHighCamNode->getPosition());

    // highcam -> lowcam
    key = mCamTrack->createNodeKeyFrame(4);
    key->setTranslate(mHighCamNode->getPosition() - mLowCamNode->getPosition());


    // add animstate
    mCamAnimState = globals::sm->createAnimationState("Camera Animation");
    mCamAnimState->setLoop(false);
}

void PlayerCam::setDefaultCamera()
{
    mCamera->setDirection(Vector3(0, 0, -1)); // default direction
    mCamera->setProjectionType(PT_PERSPECTIVE);
    mCamera->setFOVy(Degree(40));
    mCamera->yaw(Degree(-5));
    mCamera->pitch(Degree(-30));
    //cout << mCamera->getOrientation() << endl;
    //cout << mCamera->getDirection() << endl;
}

void PlayerCam::zoomStep(int step)
{
   if(step > 0) {
   }
}

void PlayerCam::setLowCam()
{
    mCamAnimState->setTimePosition(0);
    mCamAnimState->setLength(2);
    mCamAnimState->setEnabled(true);
}

void PlayerCam::setHighCam()
{
    //mCamera->setAutoTracking(true, mHighCamNode, Vector3::ZERO);
    //mCamera->setFOVy(Degree(60));
    mCamAnimState->setTimePosition(2);
    mCamAnimState->setLength(2);
    mCamAnimState->setEnabled(true);
}

void PlayerCam::mouseMoved(const OIS::MouseEvent& evt)
{
    float sx = (float) evt.state.X.abs / evt.state.width;
    float sy = (float) evt.state.Y.abs / evt.state.height;
    float margin = 0.001;

    mScrollUp = sy < margin ? true : false;
    mScrollDown = sy > 1.0 - margin ? true : false;
    mScrollLeft = sx < margin ? true : false;
    mScrollRight = sx > 1.0 - margin ? true : false;

    // setup zoom from mouse wheel which is the z axis of mouse event
    mViewZoom -= (float) evt.state.Z.rel/2.0f;
    if(mViewZoom < 2) mViewZoom = 2;
    if(mViewZoom > 500) mViewZoom = 500;
    //mCamera->moveRelative(Vector3(0, mViewZoom, 0));

    if(mRMBDown) {
        mYaw -= (float) evt.state.X.rel * 0.3;
        mPitch -= (float) evt.state.Y.rel * 0.3;
    }
    else {
        mZoomPitch = -(mViewZoom * 0.1);
    }
}

void PlayerCam::mousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id)
{
    if(id == OIS::MB_Right)
        mRMBDown = true;
}

void PlayerCam::mouseReleased(const OIS::MouseEvent& evt, OIS::MouseButtonID id)
{
    if(id == OIS::MB_Right)
        mRMBDown = false;
}

void PlayerCam::update(float dt)
{
    float ss = 200 * dt;

    Vector3 v = Vector3(0, 0, 0);
    if(mScrollUp) v.z += -ss;
    if(mScrollDown) v.z += ss;
    if(mScrollLeft) v.x += -ss;
    if(mScrollRight) v.x += ss;

    if(v.x != 0 && v.z != 0) // diagonal scroll
        v *= 1.0 / 1.4142135623730951;

    mCameraNode->translate(v);

    mCameraNode->resetOrientation();
    mCameraNode->pitch(Degree(mPitch) + Degree(mZoomPitch), Node::TS_LOCAL);
    mCameraNode->yaw(Degree(mYaw), Node::TS_WORLD);

    Vector3 curPos = mCameraNode->getPosition();
    mCameraNode->setPosition(Vector3(curPos.x, mViewZoom, curPos.z));
    //mCameraNode->translate(Vector3(0, mViewZoom, 0), Node::TS_LOCAL);

    if(mCamAnimState)
        mCamAnimState->addTime(dt);
}

