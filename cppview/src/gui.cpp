#include <Python.h>
#include <OgreStringConverter.h>
#include "app.h"
#include "gui.h"
#include "event.h"
#include "globals.h"

using namespace std;
using namespace CEGUI;

GUI::GUI(void *app)
    : mState(0),
    mSelBuilding_typename(""),
    mSelFactory_name("")
{
    mApp = (void *)app;

    // bind handlers to signal slots
    sig_onGuiShowWindow.connect(boost::bind(&GUI::onGuiShowWindow, this, _1));

    // CEGUI
    mRenderer = &CEGUI::OgreRenderer::bootstrapSystem();
    SchemeManager::getSingleton().create("WindowsLook.scheme");
    CEGUI::FontManager::getSingleton().create("gui.font");

    System::getSingleton().setDefaultFont("gui_font");
    System::getSingleton().setDefaultMouseCursor("WindowsLook", "MouseArrow");
    WindowManager &wmgr = WindowManager::getSingleton();
    mRoot = wmgr.createWindow("DefaultWindow", "root");

    quit_btn = wmgr.createWindow("WindowsLook/Button", "QuitButton");
    quit_btn->setText("Quit");
    quit_btn->setSize(UVector2(UDim(0.10, 0), UDim(0.03, 0)));
    quit_btn->setXPosition(UDim(0.05, 0));
    mRoot->addChildWindow(quit_btn);

    build_btn = wmgr.createWindow("WindowsLook/Button", "BuildButton");
    build_btn->setText("Build");
    build_btn->setSize(UVector2(UDim(0.10, 0), UDim(0.03, 0)));
    build_btn->setXPosition(UDim(0.15, 0));
    mRoot->addChildWindow(build_btn);
    build_btn->subscribeEvent(PushButton::EventClicked,
            Event::Subscriber(&GUI::build_btn_clicked_cb, this));

    unitprod_btn = wmgr.createWindow("WindowsLook/Button", "UnitProdButton");
    unitprod_btn->setText("Unit prod.");
    unitprod_btn->setSize(UVector2(UDim(0.10, 0), UDim(0.03, 0)));
    unitprod_btn->setXPosition(UDim(0.25, 0));
    mRoot->addChildWindow(unitprod_btn);
    unitprod_btn->subscribeEvent(PushButton::EventClicked,
            Event::Subscriber(&GUI::unitprod_btn_clicked_cb, this));

    // building select window
    mBSwin = wmgr.createWindow("WindowsLook/FrameWindow", "BuildingSelectionWindow");
    mBSwin->setSize(UVector2(UDim(0.50, 0), UDim(0.50, 0)));
    mBSwin->setXPosition(UDim(0.35, 0));
    mBSwin->setYPosition(UDim(0.5, 0));
    mRoot->addChildWindow(mBSwin);

    Window *b1_btn = wmgr.createWindow("WindowsLook/Button", "Building1");
    b1_btn->setText("Default");
    b1_btn->setSize(UVector2(UDim(0.30, 0), UDim(0.10, 0)));
    mBSwin->addChildWindow(b1_btn);

    Window *b2_btn = wmgr.createWindow("WindowsLook/Button", "Building2");
    b2_btn->setText("Power plant");
    b2_btn->setSize(UVector2(UDim(0.30, 0), UDim(0.10, 0)));
    b2_btn->setYPosition(UDim(0.10, 0));
    mBSwin->addChildWindow(b2_btn);

    b1_btn->subscribeEvent(PushButton::EventClicked,
            Event::Subscriber(&GUI::BSwin_build_building_clicked_cb, this));
    b2_btn->subscribeEvent(PushButton::EventClicked,
            Event::Subscriber(&GUI::BSwin_build_building_clicked_cb, this));

    mBSwin->setVisible(false);

    // add FPS win
    mFPSwin = wmgr.createWindow("WindowsLook/StaticText", "FPSWindow");
    mFPSwin->setSize(UVector2(UDim(0.06, 0), UDim(0.04, 0)));
    mFPSwin->setXPosition(UDim(0.94, 0));
    mFPSwin->setYPosition(UDim(0.0, 0));
    mFPSwin->setText("FPS counter");
    mRoot->addChildWindow(mFPSwin);

    // add unit production window
    mUnitProd = wmgr.loadWindowLayout("unitprod.layout");
    Window *win = mUnitProd->getChildRecursive("CloseButton");
    win->subscribeEvent(PushButton::EventClicked, Event::Subscriber(&GUI::hide_parent_window_cb, this));
    mUnitProd->setVisible(false);
    mRoot->addChildWindow(mUnitProd);

    System::getSingleton().setGUISheet(mRoot);

    // Move CEGUI mouse to (0,0)
    // a less hacky approach to fix cegui cursor centering issue
    //CEGUI::Point mousePos = CEGUI::MouseCursor::getSingleton().getPosition();  
    //CEGUI::System::getSingleton().injectMouseMove(-mousePos.d_x,-mousePos.d_y);

    cout << "GUI initialized." << endl;
}

bool GUI::UnitProd_slotx_clicked_cb(const CEGUI::EventArgs& e)
{
    cout << "!!!!!" << endl;
    //Window *slotx = (static_cast<const WindowEventArgs&>(e).window);
    //slotx->setVisible(false);
    Window *slotx = (static_cast<const WindowEventArgs&>(e).window);
    //slotx->setVisible(false);

    cout << slotx->getName() << endl;
    Window *counter = slotx->getChildRecursive(slotx->getName() + "_counter");
    counter->setText("33");
    cout << counter->getText() << endl;

    cout << mSelFactory_name << endl;

    // get prodq
    //PyObject *facname_ = PyString_FromString(mSelFactory_name.c_str());
    PyObject *prodq = PyMapping_GetItemString(m_prodqs, const_cast<char*>(mSelFactory_name.c_str()));
    //Py_DECREF(facname_);

    PyObject *ret = PyObject_CallMethod(m_unitprod, "onUnitProdSlotxClick", "s", slotx->getName().c_str());
    if (!ret) {
        PyErr_Print();
        exit(-1);
    }
    Py_DECREF(ret);

    Py_DECREF(prodq);
    return true;
}

// UnitProd
void GUI::UnitProd_show_factory(char *facname)
{
    // python
    m_prodman = PyObject_GetAttrString(globals::game_obj, "prodman");
    m_prodqs = PyObject_GetAttrString(m_prodman, "prodqs");
    m_gui = PyObject_GetAttrString(globals::game_obj, "gui");
    m_unitprod = PyObject_GetAttrString(m_gui, "unitprod");

    mSelFactory_name = std::string(facname);
    PyObject *ret = PyObject_CallMethod(m_unitprod, "build", "s", facname);
    Py_DECREF(ret);

    PyObject *unit_slots = PyObject_GetAttrString(m_unitprod, "unit_slots");

    // mil_units
    for(int i=0; i < PyList_Size(unit_slots); i++) {
        // fetch data
        bool listed = false;
        bool avail = false;
        char *image;

        PyObject *unit_slot = PyList_GetItem(unit_slots, i);

        avail = PyObject_IsTrue(PyObject_GetAttrString(unit_slot, "avail"));
        listed = PyObject_IsTrue(PyObject_GetAttrString(unit_slot, "listed"));
        image = PyString_AsString(PyObject_GetAttrString(unit_slot, "image"));

        // process buttons
        Window *slotx = mUnitProd->getChildRecursive("mil_units_slot" + StringConverter::toString(i));
        Window *slotx_img = slotx->getChildRecursive("mil_units_slot" + StringConverter::toString(i) + "_img");
        //slotx->setImage();

        if(listed) {
            if(avail) {
                slotx_img->setProperty("Image", image);
                slotx->subscribeEvent(PushButton::EventClicked, Event::Subscriber(&GUI::UnitProd_slotx_clicked_cb, this));
                //slotx->subscribeEvent(PushButton::EventClicked, Event::Subscriber(&GUI::hide_parent_window_cb, this));
            }
            else {
                slotx_img->setProperty("Image", image);
            }
        }
        else {
            //slotx->setVisible(false);
        }
    }

    mUnitProd->setVisible(true);
}

#if 0
// UnitProd
void GUI::UnitProd_show_factory(char *facname)
{
    mSelFactory_name = facname;

    PyObject *facname_ = PyString_FromString(facname);
    PyObject *prodq = PyObject_GetItem(prodqs, facname_);
    Py_DECREF(facname_);

    // list of possible prodtypes
    std::vector<string> v;
    v.push_back("lighttank");
    v.push_back("wheeledtank");
    v.push_back("dumptruck");

    // mil_units
    for(int i=0; i < v.size(); i++) {
        string typename_ = v[i];
        bool listed = false;
        bool avail = false;

        PyObject *ret = PyObject_CallMethod(prodq, "ableProduce", "s", typename_.c_str());
        //PyObject_Print(ret, stderr, 0);
        listed = PyObject_IsTrue(ret);
        Py_DECREF(ret);

        if(listed) {
            PyObject *ret = PyObject_CallMethod(prodq, "canProduceNow", "s", typename_.c_str());
            //PyObject_Print(ret, stderr, 0);
            avail = PyObject_IsTrue(ret);
            Py_DECREF(ret);
        }

        // process buttons
        Window *slotx = mUnitProd->getChildRecursive("mil_units_slot" + StringConverter::toString(i));
        Window *slotx_img = slotx->getChildRecursive("mil_units_slot" + StringConverter::toString(i) + "_img");
        //slotx->setImage();

        if(listed) {
            if(avail) {
                char strProperty[40];
                sprintf(strProperty, "set:UnitIcons image:%s", typename_.c_str());
                slotx_img->setProperty("Image", strProperty);

                slotx->subscribeEvent(PushButton::EventClicked, Event::Subscriber(&GUI::UnitProd_slotx_clicked_cb, this));
                //slotx->subscribeEvent(PushButton::EventClicked, Event::Subscriber(&GUI::hide_parent_window_cb, this));
            }
            else {
                slotx_img->setProperty("Image", "set:UnitIcons image:unavail");
            }
        }
        else {
            //slotx->setVisible(false);
        }
    }

    Py_DECREF(prodq);

    mUnitProd->setVisible(true);
}
#endif

void GUI::hideAllWindows()
{
    mBSwin->setVisible(false);
    mUnitProd->setVisible(false);
}

bool GUI::inFocus()
{
    CEGUI::Vector2 mousePos = CEGUI::MouseCursor::getSingleton().getPosition();
    CEGUI:: Window *w = mRoot->getChildAtPosition(mousePos);

    // hacks
    if(w == mFPSwin)
        return false;

    return (w) ? true : false;
}

bool GUI::hide_parent_window_cb(const CEGUI::EventArgs& e)
{
    (static_cast<const WindowEventArgs&>(e).window)->getParent()->setVisible(false);
    return true;
}

bool GUI::build_btn_clicked_cb(const CEGUI::EventArgs& e)
{
    mBSwin->setVisible(true);
    return true;
}

bool GUI::unitprod_btn_clicked_cb(const CEGUI::EventArgs& e)
{
    mUnitProd->setVisible(true);
    return true;
}

bool GUI::BSwin_build_building_clicked_cb(const CEGUI::EventArgs& e)
{
    CEGUI::String bname = (static_cast<const WindowEventArgs&>(e).window)->getName();
    if(bname == "Building1") {
        mSelBuilding_typename = "default";
    }
    else if(bname == "Building2") {
        mSelBuilding_typename = "power_plant";
    }

    switchState(States::place_building); 
    mBSwin->setVisible(false);

    return true;
}

void GUI::switchState(int newstate)
{
    if(newstate == States::place_building && mState == States::idle)
    {
        // place building state
        globals::field->showMarker = true;
        mState = newstate;
    }
    else if(newstate == States::idle && mState == States::place_building)
    {
        // idle state
        globals::field->showMarker = false;
        mState = newstate;
    }
}

void GUI::mousePressed()
{
    if(mState == States::place_building) {
        // fire event
        PyObject *kwlist = Py_BuildValue("{s:s}", "building_typename", mSelBuilding_typename.c_str());
        send_event("ev_gui_place_building", kwlist);
        Py_DECREF(kwlist);

        // finalize: switch state back
        switchState(States::idle);
    }
}

void GUI::setCursor(char *name)
{
    MouseCursor::getSingleton().setImage("WindowsLook", CEGUI::String(name));
}

void GUI::update(float dt)
{
    const Ogre::RenderTarget::FrameStats& stats = ((App *)mApp)->mWindow->getStatistics();
    char s[10];
    sprintf(s, " %.2f", stats.lastFPS);
    mFPSwin->setText(s);
}


CEGUI::MouseButton _convertButton(OIS::MouseButtonID buttonID)
{
    switch (buttonID)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;
        break;
 
    case OIS::MB_Right:
        return CEGUI::RightButton;
        break;
 
    case OIS::MB_Middle:
        return CEGUI::MiddleButton;
        break;
 
    default:
        return CEGUI::LeftButton;
        break;
    }
}

void GUI::onGuiShowWindow(PyObject *args)
{
    PyObject *slot_ = PySequence_GetItem(args, 0);
    char *name = PyString_AsString(slot_);
    Py_DECREF(slot_);

    if(strcmp(name, "UnitProd") == 0) {
        cout << "LOL" << endl;
        PyObject *hud = PyObject_GetAttrString(globals::game_obj, "hud");
        PyObject *slot1_ = PyObject_GetAttrString(hud, "selectedBuilding");
        PyObject_Print(slot1_, stderr, 0);
        PyObject *slot2_ = PyObject_GetAttrString(slot1_, "name");
        char *b_name_ = PyString_AsString(slot2_);
        cout << "b_name_" << b_name_ << endl;
        Py_DECREF(slot2_);
        Py_DECREF(slot1_);

        UnitProd_show_factory(b_name_);
    }

    Window *win = mRoot->getChildRecursive(name);
    win->setVisible(true);
}
