#ifndef GUI_H
#define GUI_H
#include <OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>
#include <boost/signals2/signal.hpp>
#include "event.h"

class GUI
{
private:
    void UnitProd_show_factory(char *facname);
    PyObject *m_prodman;
    PyObject *m_prodqs;
    PyObject *m_unitprod;
    PyObject *m_gui;
protected:
    bool build_btn_clicked_cb(const CEGUI::EventArgs& e);
    bool unitprod_btn_clicked_cb(const CEGUI::EventArgs& e);
    bool hide_parent_window_cb(const CEGUI::EventArgs& e);
    bool BSwin_build_building_clicked_cb(const CEGUI::EventArgs& e);
    bool UnitProd_slotx_clicked_cb(const CEGUI::EventArgs& e);
    void *mApp;
    std::string mSelFactory_name;
public:
    int mState;
    struct States {
        enum { idle, place_building };
    };
    CEGUI::String mSelBuilding_typename;
    CEGUI::OgreRenderer* mRenderer;

    CEGUI::Window *quit_btn, *build_btn, *unitprod_btn, *mRoot, *mBSwin, *mFPSwin;
    CEGUI::Window *mUnitProd;

    GUI(void *app);
    void update(float dt);
    void switchState(int newstate);

    void mousePressed();
    void setCursor(char *name);
    bool inFocus();
    void hideAllWindows();

    // signals
    void onGuiShowWindow(PyObject *args);
    SIG sig_onGuiShowWindow;
};

CEGUI::MouseButton _convertButton(OIS::MouseButtonID buttonID);

#endif
