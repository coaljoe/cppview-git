#ifndef OBJECT_H
#define OBJECT_H
#include "Ogre.h"
#include "physobj.h"

using namespace Ogre;

class Obj
{
protected:
    SceneNode* mPivot;
public:
    bool spawned;
    Vector3 m_pos;
    Quaternion m_ori;
    PhysObj phys;

    // Graphics
    char *m_meshFile;
    char *m_materialName;
    void setMesh(const char *s);
    void setMaterial(const char *s);
    char* getMesh() { return m_meshFile; }
    SceneNode* getSceneNode() { return mPivot; }
    
    // Methods
    Obj(Ogre::Vector3 pos);
    Obj();
    Obj(const Obj &oth) {
        *this = oth;
        if(oth.spawned) // XXX unset spawned flag
          this->spawned = false;
    }
    void copy_spawn(const Obj &oth) {
        *this = oth;
        if(oth.spawned) {
            this->spawned = false;
            this->spawn();
        }
    }

    Vector3 getPos() { return m_pos; }
    void setPos(Vector3 pos) {
        m_pos = pos;
        if(spawned)
            mPivot->setPosition(m_pos);
            // XXX fixme: doens't set phys' objs position
    }

    void setOri(Quaternion ori) {
        m_ori = ori;
        if(spawned)
            mPivot->setOrientation(m_ori); // XXX fixme
    }

    void translate(Vector3 v) {
        mPivot->translate(v);
    }

    void yaw(Radian angle, int ts) {
        Node::TransformSpace ts_ = static_cast<Node::TransformSpace>(ts);
        mPivot->yaw(angle, ts_);
    }

    void setDirection(const Vector3 &vec, int ts, const Vector3 &localDirectionVector=Vector3::NEGATIVE_UNIT_Z) {
        Node::TransformSpace ts_ = static_cast<Node::TransformSpace>(ts);
        mPivot->setDirection(vec, ts_, localDirectionVector);
    }

    void addNode(const char *name, const char *mesh, Vector3 pos, Quaternion ori);
    void rotate(Vector3 axis, Radian angle);
    void spawn();
    //void clone();
    void update(float dt) {}
};

/*
class StaticObj
{
};
*/

#endif
