#include <Python.h>
#include <Ogre.h>
#include <OgreOggSound.h>
#include "audioobj.h"

using namespace Ogre;
using namespace OgreOggSound;
using namespace std;

// static
OgreOggSound::OgreOggSoundManager *soundManager;

void init_sound()
{
    cout << "init sound" << endl;
    soundManager = OgreOggSoundManager::getSingletonPtr();
    if(!soundManager->init()) {
        cout << "audio init failed" << endl;
        exit(-1);
    }
    if(!soundManager->getListener()) {
        cout << "XXXXXXXXXX can get listener" << endl;
        exit(-1);
    }
    //soundManager->createSound("tank_fire", "tank_fire.ogg", false, false, true);
    //soundManager->getSound("tank_fire")->play();
    cout << "init sound ok" << endl;
}

AudioObj::AudioObj()
{
}

void AudioObj::onUnitFire(PyObject *args)
{
    cout << "AudioObj: onUnitFire" << endl;
}

void playSound()
{
    cout << "Audio: playsound" << endl;
}
