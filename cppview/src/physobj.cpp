#include "Ogre.h"
#include "btBulletDynamicsCommon.h"
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"
#include "physics.h"
#include "physobj.h"

using namespace Ogre;
using namespace std;

PhysObj::PhysObj()
{
    mass = 0.0f;
    friction = 1.0f;
    inertia = btVector3(0, 0, 0);
    shapeType = COLSHAPE_BOX;
}

void PhysObj::setLinearVelocity(Vector3 v)
{
    btVector3 _v = BtOgre::Convert::toBullet(v);
    body->setLinearVelocity(_v);
}

void PhysObj::translate(Vector3 v)
{
    btVector3 _v = BtOgre::Convert::toBullet(v);
    body->translate(_v);
}

void PhysObj::spawn(void *_node, Entity *en)
{
    //Entity *en = static_cast<Entity*>(node->getAttachedObject(0));
    SceneNode *node = static_cast<SceneNode*>(_node);
    BtOgre::StaticMeshToShapeConverter converter(en);

    if(shapeType == COLSHAPE_BOX)
        shape = converter.createBox();
    else if(shapeType == COLSHAPE_SPHERE)
        shape = converter.createSphere();
    else if(shapeType == COLSHAPE_CYLINDER)
        shape = converter.createCylinder();
    else if(shapeType == COLSHAPE_CONVEX)
        shape = converter.createConvex();
    else if(shapeType == COLSHAPE_TRIMESH)
        shape = converter.createTrimesh();
    shape->calculateLocalInertia(mass, inertia);
    physics::m_collisionShapes.push_back(shape);

    BtOgre::RigidBodyState* mots = new BtOgre::RigidBodyState(node);

    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, mots, shape, inertia);
    body = new btRigidBody(rbInfo);
    body->setActivationState(DISABLE_DEACTIVATION);
    body->setFriction(friction);
    physics::m_dynamicsWorld->addRigidBody(body);
}

