#include <Python.h>
#include <Ogre.h>
#include "fx.h"
#include "globals.h"

FX* FX::m_pInstance = NULL; 

FX* FX::Instance()
{
    if (!m_pInstance)  
        m_pInstance = new FX;

    return m_pInstance;
}

void FX::addExplosion(Vector3 pos, float angle)
{
    String rnd = StringConverter::toString(rand()); // fixme
    ParticleSystem *ps = globals::sm->createParticleSystem("explotion_" + rnd, "explosionTemplate");
    // fast forward 1 second  to the point where the particle has been emitted
    ps->fastForward(1.0);
    ps->setDefaultDimensions(6, 6);

    if(angle)
        ps->setParameter("angle", StringConverter::toString(angle));

    SceneNode *sn = globals::sm->getRootSceneNode()->createChildSceneNode();
    sn->setPosition(pos);
    sn->attachObject(ps);

}

void FX::addHit(Vector3 pos)
{
    String rnd = StringConverter::toString(rand()); // fixme
    ParticleSystem *ps = globals::sm->createParticleSystem("explotion_" + rnd, "explosionTemplate");
    // fast forward 1 second  to the point where the particle has been emitted
    ps->fastForward(1.0);
    ps->setDefaultDimensions(12, 12);

    SceneNode *sn = globals::sm->getRootSceneNode()->createChildSceneNode();
    sn->setPosition(pos);
    sn->attachObject(ps);

}

void FX::addSmoke(Vector3 pos)
{
    String rnd = StringConverter::toString(rand()); // fixme
    SceneNode *sn = globals::sm->getRootSceneNode()->createChildSceneNode();
    sn->setPosition(pos);
    sn->attachObject(globals::sm->createParticleSystem("smoke_" + rnd, "UnitSmoke"));
}
