#include <iostream>
#include "Ogre.h"
#include "btBulletDynamicsCommon.h"
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "obj.h"
#include "physics.h"

using namespace std;
using namespace physics;

namespace physics
{

BtOgre::DebugDrawer *dbgdraw;
btAlignedObjectArray<btCollisionShape*> m_collisionShapes;
btBroadphaseInterface*  m_broadphase;
btCollisionDispatcher*  m_dispatcher;
btConstraintSolver*     m_solver;
btDefaultCollisionConfiguration* m_collisionConfiguration;
btDynamicsWorld*        m_dynamicsWorld;
bool debug;

void init()
{
    printf("physics::init()\n");
    ///collision configuration contains default setup for memory, collision setup
    m_collisionConfiguration = new btDefaultCollisionConfiguration();
    //m_collisionConfiguration->setConvexConvexMultipointIterations();

    ///use the default collision dispatcher.
    //For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    m_dispatcher = new btCollisionDispatcher(m_collisionConfiguration);

    m_broadphase = new btDbvtBroadphase();

    ///the default constraint solver.
    //For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    btSequentialImpulseConstraintSolver* sol = new btSequentialImpulseConstraintSolver;
    m_solver = sol;

    m_dynamicsWorld = new btDiscreteDynamicsWorld(m_dispatcher,m_broadphase,m_solver,m_collisionConfiguration);

    m_dynamicsWorld->getDispatchInfo().m_enableSPU = false;
    m_dynamicsWorld->setGravity(btVector3(0,-10,0));

    ///create a few basic rigid bodies
    btCollisionShape* groundShape = new btBoxShape(btVector3(btScalar(50.),btScalar(50.),btScalar(50.)));
    //      btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0,1,0),50);

    m_collisionShapes.push_back(groundShape);

    btTransform groundTransform;
    groundTransform.setIdentity();
    groundTransform.setOrigin(btVector3(0,-50,0));

    debug = false;
}

void deinit()
{
    //cleanup in the reverse order of creation/initialization

    //remove the rigidbodies from the dynamics world and delete them
    int i;
    for (i=m_dynamicsWorld->getNumCollisionObjects()-1; i>=0 ;i--)
    {
            btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
            btRigidBody* body = btRigidBody::upcast(obj);
            if (body && body->getMotionState())
            {
                    delete body->getMotionState();
            }
            m_dynamicsWorld->removeCollisionObject( obj );
            delete obj;
    }

    //delete collision shapes
    for (int j=0;j<m_collisionShapes.size();j++)
    {
            btCollisionShape* shape = m_collisionShapes[j];
            delete shape;
    }

    delete m_dynamicsWorld;
    delete m_solver;
    delete m_broadphase;
    delete m_dispatcher;
    delete m_collisionConfiguration;
    
    delete dbgdraw;
}


void update(float dt)
{
    //m_dynamicsWorld->stepSimulation(dt / 100.f);
    m_dynamicsWorld->stepSimulation(dt, 7);
    if(debug) {
        m_dynamicsWorld->debugDrawWorld();
        dbgdraw->step();
    }
}

void debug_init(SceneNode *node)
{
    dbgdraw = new BtOgre::DebugDrawer(node, m_dynamicsWorld);
    m_dynamicsWorld->setDebugDrawer(dbgdraw);
    dbgdraw->setDebugMode(debug);
}

void addFloor()
{
    btCollisionShape* floorShape = new btBoxShape(btVector3(50, 0, 50));
    m_collisionShapes.push_back(floorShape);

    btTransform floorTransform;
    floorTransform.setIdentity();
    floorTransform.setOrigin(btVector3(0, 0, 0));

    btScalar mass = 0.0;
    btVector3 inertia = btVector3(0, 0, 0);

    btDefaultMotionState* mots = new btDefaultMotionState(floorTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, mots, floorShape, inertia);
    btRigidBody *body = new btRigidBody(rbInfo);
    m_dynamicsWorld->addRigidBody(body);
}

Hinge::Hinge(Obj *ob1, Obj *ob2, Quaternion basisA, Vector3 originA,
                                 Quaternion basisB, Vector3 originB)
{
    btTransform localA, localB;
    localA.setIdentity();
    localB.setIdentity();
    /*
    localA.getBasis().setEulerZYX(0, 0, M_PI_2);
    localA.setOrigin(btVector3(0.0, 0.0, 0.0));
    localB.getBasis().setEulerZYX(0, 0, M_PI_2);
    localB.setOrigin(btVector3(0.0, 0.0, 0.0));
    */
    localB.getBasis().setRotation(BtOgre::Convert::toBullet(basisA));
    localA.setOrigin(BtOgre::Convert::toBullet(originA));
    localA.getBasis().setRotation(BtOgre::Convert::toBullet(basisB));
    localB.setOrigin(BtOgre::Convert::toBullet(originB));

    hinge = new btHingeConstraint(*ob1->phys.body,*ob2->phys.body, localA, localB);
    //ob1->phys.setConstraint(hinge);
    //ob2->phys.setConstraint(hinge);
    m_dynamicsWorld->addConstraint(hinge, true); // should be last


    cout << "addHinge with " << ob1 << " " << ob2 << " "
                             << basisA << " " << originA << " "
                             << basisB << " " << originB << endl;
}

void Hinge::lock()
{
    hinge->setLimit(0, 0);
}


void Hinge::rotateAngular(float vel, float impulse)
{
    float l = 1745;
    hinge->setLimit(-l, l);
    hinge->enableAngularMotor(true, vel, impulse);
}

void Hinge::rotateToAngle(float angle)
{
    hinge->setMaxMotorImpulse(5); // power
    cout << "LOL" << endl;
    cout << hinge->getMaxMotorImpulse();
    //float _angle = -1.1; // velocity
    //float _angle = 0.8; // velocity
    //float _angle = angle; // velocity
    float _angle = hinge->getHingeAngle() + angle; // velocity
    hinge->setMotorTarget((btScalar)_angle, (btScalar)0.1);
    hinge->enableMotor(true); 
    //hinge->setAngularOnly(true);

    /*
    float   targetVelocity = 12.f;
    float   maxMotorImpulse = 1.0f;
    hinge->enableAngularMotor(true,targetVelocity,maxMotorImpulse);
    */
}

void Hinge::enableMotor(bool val)
{
    hinge->enableMotor(val);
}

float Hinge::getHingeAngle()
{
    return (float)hinge->getHingeAngle();
}

} // ::physics 

