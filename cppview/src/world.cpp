#include <vector>
#include "obj.h"
#include "world.h"
#include "globals.h"

namespace world
{
std::vector<Obj*> m_objs;

void add_obj(Obj* o)
{
    m_objs.push_back(o);
}

void update(float dt)
{
    for(int i; i < m_objs.size(); i++) {
        m_objs[i]->update(dt);
    }

    globals::dt = dt;
}

} // ::world
