#ifndef FIELD_H
#define FIELD_H
#include <Python.h>
#include <Ogre.h>

using namespace Ogre;

class Field
{
protected:
    SceneNode *marker;
    PyObject *mObj;
public:
    int pointer_x;
    int pointer_y;
    Vector3 pointerPos;
    bool showMarker;
    SceneNode *mFieldNode;

    // Methods
    Field();
    ~Field();
    void load(const char *s);

    void spawn();
    void update(float dt);
    void test();
    float getHeightAtCell(int x, int y);
    void setPointerPos(Vector3);

    void set_model(PyObject *o) { mObj = o; }
};

#endif
