#!/bin/sh
##
## redist directory: boost_1_44_0, bullet, CEGUI-SDK, ogreoggsound, OgreSDK, Python27 
##
dst=`pwd`/build/dist-test
pydir=c:/Python27


mkdir -p $dst/redist/pylib
pushd $pydir/Lib/site-packages
cp -r sip.pyd sipconfig.py sipdistutils.py \
      message-0.1.2-py2.7.egg \
      numpy \
      $dst/redist/pylib
popd

pushd $dst/redist/pylib
unzip message-0.1.2-py2.7.egg message/*
popd

echo "** Now copy c:\Python27\Lib manually to $dst/redist/pylib"

#rm -rf $dst/*
cp -r --parents redist/OgreSDK/media/models $dst
cp -r --parents redist/OgreSDK/media/RTShaderLib $dst
cp -r --parents redist/OgreSDK/bin/release $dst
cp -r --parents redist/CEGUI-SDK/bin $dst
cp -r --parents redist/CEGUI-SDK/datafiles $dst

rm -rf redist/CEGUI-SDK/bin/ReleaseWithSymbols
rm -f redist/CEGUI-SDK/bin/*.pdb
rm -f redist/OgreSDK/bin/release/sample*
rm -f redist/OgreSDK/bin/release/Sample*

# copy project files
cp -r --parents config data media src tests $dst
cp -r --parents "cppview" $dst
mv $dst/cppview.exe $dst/cppview

cp *.cfg *.bat $dst
cp cppview.exe cppview.exe.manifest cppview.pyd $dst

# cleanup project
rm -rf $dst/cppview/.obj
find $dst -iname \*.py[co] -exec rm -f {} \;
rm -f $dst/*.log
find $dst/redist/CEGUI-SDK/bin/ -iname \*_d\* -exec rm -f {} \;

# final steps
cp redist/ogreoggsound/lib/OgreOggSound.dll $dst/redist/OgreSDK/bin/release
cp $WINDIR/system32/python27.dll $dst
cp $pydir/python.exe $dst

