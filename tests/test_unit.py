from __future__ import division, absolute_import
from src.units.tank import Tank
from src.unit import Unit, States
from src import conf
conf.vars['disable_views'] = True

def test_tank():
    u1 = Tank(0, 0)

    u1.moveTo(1, 1)
    assert len(u1.path) > 0

def test_unit():
    u1 = Unit(0, 0)
    #u1.spawn()

    assert u1.state == States.idle

    # test state changing

    u1.state = States.unloading

    assert u1.state == States.unloading

    def _test(ob):
        assert ob.state == States.unloading

    _test(u1)

