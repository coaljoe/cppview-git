from __future__ import division, absolute_import
from src import conf
conf.vars['disable_views'] = True
from src.game import Game
from src.unit import States
from src.units.dumptruck import DumpTruck

game = Game()
game.speed = 1.0

def test_dumptruck():
    u1 = DumpTruck(0, 0)
    game.objs.append(u1)

    u1.dumpBodyAngleInc = 5.0
    u1.state = States.unloading
    u1.unloadCargoState()
    #u1.handle_func(0.1)
    #u1._handle_unloading(0.1)
    print u1.dumpBodyAngle

    #while game.gameloop.hasTasks:
    #while u1.handle_func:
    while game.frame < 20:
        game.update(0.1)

    assert u1.dumpBodyAngle == 0
    assert u1.state == States.idle
    
