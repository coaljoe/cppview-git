from __future__ import division, absolute_import
from src import conf
conf.vars['disable_views'] = True
from src.game import Game
from src.prodman import ProductionManager
from src.buildings.factory import Factory

game = Game()

def test_prodman():
    pm = game.prodman
    factory = Factory()
    factory.spawn()
    #print game.event.event_queue
    #game.update(1)
    #print game.event.event_queue
    #pm.addProdq('TestQueue', factory)
    #ret = pm.buyUnit('tank', 'TestQueue')
    ret = pm.buyUnit('lighttank', factory.name)

    #print pm.errmsg
    assert ret is True

    game.player.money = 0
    ret = pm.buyUnit('lighttank', factory.name)

    assert ret is False
