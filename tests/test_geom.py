from __future__ import division, absolute_import
from src import geom
from src import conf
conf.vars['disable_views'] = True

def test_geom():

    # a: unit, b: target, c: point on circle with range r

    ax, ay = 1, 1
    bx, by = 10, 1
    r = 5

    cx, cy = geom.point_on_radius(ax, ay, bx, by, r)

    print cx, cy

