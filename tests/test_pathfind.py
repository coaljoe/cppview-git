from __future__ import division, absolute_import
from src.pathfind import path
from src import conf
conf.vars['disable_views'] = True

def test_path():
    print "\n0, 0 => 1, 1"
    p = path((0, 0), (1, 1))
    print p

    assert  p[0].x == 1 and \
            p[0].y == 1

    print "\n1, 1 => 3, 3"
    p = path((1, 1), (3, 3))
    print p # should be reversed b -> a

    print "\n4, 19 => 4, 9"
    p = path((4, 19), (4, 9))
    print p
