from __future__ import division, absolute_import

class PlayerManager(object):
    def __init__(self):
        self.players = []
        self.player = None # current player

        self._prev_player = None

    def addPlayer(self, player):
        self.players.append(player)

        # set default player
        if len(self.players) == 1:
            self.player = self.players[0]

    def swapPlayer(self, player=None):
        """Swaps current player with player"""
        if player:
            self._prev_player = self.player
            self.player = player
        else:
            self.player = self._prev_player
            self._prev_player = None

class Player(object):
    def __init__(self, name, type='human', camp='reds'):
        self.name = name
        self.type = type # ai, human
        self.camp = camp # reds, arctic, suur

        self.money = 1000
        self.iron = 1000
        self.oil = 1000
        self.pop = 1000


