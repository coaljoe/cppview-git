#!/usr/bin/python
from __future__ import division, absolute_import
import os, sys
from optparse import OptionParser
from time import clock

from OpenGL.GL import *
from src.lib import glfw
from src.lib import h3d


class Window(object):
    def __init__(self, app):
        self._app = app

        self._width = 800
        self._height = 600
        self._mouse_prev_x = 0
        self._mouse_prev_y = 0
        self.has_exit = False

        glfw.Init()
        glfw.Enable(glfw.STICKY_KEYS)
        glfw.OpenWindow(800, 600, 8, 8, 8, 8, 24, 8, glfw.WINDOW)
        glfw.SwapInterval(0)
        glfw.SetWindowPos(100, 100)
        glfw.Disable(glfw.MOUSE_CURSOR)
 
        #glfw.SetWindowRefreshCallback(self.on_refresh)
        glfw.SetWindowSizeCallback(self.on_resize)
        glfw.SetMouseButtonCallback(self.on_mouse_button)
        glfw.SetMousePosCallback(self._on_mouse_pos)
        glfw.SetKeyCallback(self.on_key)
 

    def on_resize(self, width, height):
        self._width = width
        self._height = height

    @property
    def camera(self):
        return self._app.camera

    def _on_mouse_pos(self, x, y):
        dx = x - self._mouse_prev_x
        dy = y - self._mouse_prev_y
        self._mouse_prev_x = x
        self._mouse_prev_y = y
        self.on_mouse_pos(x, y, dx, dy)

    def on_mouse_pos(self, x, y, dx, dy):
        pass

    def on_mouse_button(self, button, pressed):
        pass

    def on_key_press(self, key):
        pass

    def on_key(self, key, pressed):
        if pressed:
            self.on_key_press(key)

    def isPressed(self, key):
        v = True if glfw.GetKey(key) == glfw.GLFW_PRESS else False
        #if v: print key
        return v


class App(object):
    def __init__(self, windowCls):
        self.w = windowCls(self)

    def init(self):
        self._initHorde3D()
        self._dt = 0.0
        self._lastTime = clock()

    def _initHorde3D(self):
        # init Horde3D
        glPushAttrib(GL_ALL_ATTRIB_BITS)

        h3d.init()

        glClearDepth(1.)
        glClearColor(0., 0., 0.5, 0.)
        glEnable(GL_DEPTH_TEST)
        
        # engine options
        h3d.setOption(h3d.Options.LoadTextures, 1)
        h3d.setOption(h3d.Options.TexCompression, 0)
        h3d.setOption(h3d.Options.FastAnimation, 0)
        h3d.setOption(h3d.Options.MaxAnisotropy, 0)
        h3d.setOption(h3d.Options.ShadowMapSize, 1024)

        # add resources
        class H3DRes:
            pass
        h3dres = H3DRes()
        self._h3dres = h3dres

        h3dres.forwardPipe = h3d.addResource(h3d.ResTypes.Pipeline, "pipelines/forward.pipeline.xml", 0)
        h3dres.fontMat = h3d.addResource(h3d.ResTypes.Material, "overlays/font.material.xml", 0)
        h3dres.logoMat = h3d.addResource(h3d.ResTypes.Material, "overlays/logo.material.xml", 0)

        # load resources from disk
        if not h3d.utils.loadResourcesFromDisk('Content'):
            print 'loading of some resources failed: See Horde3D_Log.html'

        # camera
        self.camera = h3d.addCameraNode(h3d.RootNode, "cam", h3dres.forwardPipe)
        h3d.setNodeTransform(self.camera, 0, 0, -100, 0, 0, 0, 1, 1, 1)

        width = self.w._width
        height = self.w._height
        h3d.setNodeParamI(self.camera, h3d.Camera.ViewportXI, 0)
        h3d.setNodeParamI(self.camera, h3d.Camera.ViewportYI, 0)
        h3d.setNodeParamI(self.camera, h3d.Camera.ViewportWidthI, width)
        h3d.setNodeParamI(self.camera, h3d.Camera.ViewportHeightI, height)
        #h3d.setupViewport(0, 0, width, height, True)

        h3d.setupCameraView(self.camera, 45, width / float(height), 0.1, 1000)

        self._h3dAddResources()
        self._h3dSetupScene()

    #def _h3dAddResources(self): pass
    #def _h3dSetupScene(self): pass

    def step(self):
        if self.w.has_exit:
            return False

        glfw.PollEvents()
        thisTime = clock()
        dt = (thisTime - self._lastTime)
        self._lastTime = thisTime
        self._dt = dt
        #print dt

        self._mainloopUpdate(dt)

        self._mainloopRenderOverlays(dt)
        self._mainloopRender(dt)

        h3d.finalizeFrame()
        #glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        
        """
        glPopAttrib()

        glBegin(GL_QUADS)
        glVertex3f(0, 0, 0)
        glVertex3f(0.1, 0.2, 0.3)
        glVertex3f(0.1, 0.2, 0.3)
        glEnd()

        glPushAttrib(GL_ALL_ATTRIB_BITS)
        """
        #glfw.SwapBuffers()

        h3d.utils.dumpMessages()

    def begin_custom_gl(self):
        glPopAttrib()

    def end_custom_gl(self):
        glPushAttrib(GL_ALL_ATTRIB_BITS)

    def flip(self):
        glfw.SwapBuffers()

    @property
    def running(self):
        return True if not self.w.has_exit else False

    def mainloop(self):
        self.step()

    def _mainloopUpdate(self, dt):
        pass

    def _mainloopRenderOverlays(self, dt):
        h3d.clearOverlays()

        x = self.w._width / float(self.w._height)
        verts = [
            x - 0.4, 0.8, 0.0, 1.0,
            x - 0.4, 1.0, 0.0, 0.0,
            x, 1.0, 1.0, 0.0,
            x, 0.8, 1.0, 1.0,
        ]
        h3d.showOverlays(verts, 1, 1, 1, 1, self._h3dres.logoMat, 0)


    def _mainloopRender(self, dt):
        h3d.render(self.camera)


def main():
    app = App(Window)
    app.init()
    app.mainloop()


if __name__ == '__main__':
    main()



