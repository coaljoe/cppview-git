from __future__ import division, absolute_import 
from src.views.tankview import TankView
from src.views.dumptrackview import DumpTruckView
#from src.event import event

class ViewMgr(object):
    def __init__(self, game):
        self.views = []
        self.game = game

        event = self.game.event
        event.sub('ev_unit_spawn', self.onUnitSpawn)
        #event.sub('ev_respawn', self.onRespawn)
        #event.sub('ev_destroy', self.onDestroy)

    def onUnitSpawn(self, u):
        if u.typename in ('tank', 'lighttank'):
            v = TankView(u)
            v.spawn()
        elif u.typename in ('dump_truck'):
            v = DumpTruckView(u)
            v.spawn()
        self.views.append(v)

    def update(self, dt):
        for x in self.views:
            x.update(dt)
