from __future__ import division, absolute_import
import os.path
from math import sin, cos
from vector3 import Vector3
from src import util
from src.entity import Entity
from src.game import Game
from src.const import *

class Building(Entity):
  typename = 'building_simple1'
  place_size = (10, 2)
  def __init__(self, x=0, y=0):
      Entity.__init__(self)
      self.name = util.get_autoname(self.typename)
      self.x = x # field's cell x
      self.y = y # field's cell y
      self.pos = None

  def spawn(self):
      field = Game().field
      self.pos = Vector3(self.x * cell_size, 0, -self.y * cell_size)

      for y in xrange(self.place_size[1]):
          for x in xrange(self.place_size[0]):
              print '>', self.x + x, self.y + y
              field.landmap[self.x + x][self.y + y] = 1 # filled

      #print field.landmap
      #import numpy as np
      #print np.flipud(np.rot90(field.landmap))
       
      Game().prodman._onBuildingSpawn(self)
      Game().event.pub('ev_building_spawn', self)

  def update(self, dt):
      pass


