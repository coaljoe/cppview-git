from __future__ import division, absolute_import
from math import sin, cos
#from cppview import Vector3, Degree
from vector3 import Vector3
from src import conf, pathfind, geom, util
from src.entity import Entity
from src.game import Game, timer
from src.contrib import fsm
from src.const import *
event = Game().event

class States:
    idle = 0
    moving = 1
    loading = 2
    unloading = 3

class UnitFSM(fsm.StateMachine):

    def __init__(self, obj):
        self.obj = obj
        self.dt = None
        fsm.StateMachine.__init__(self)

    @fsm.state
    def idle_state():
        def start(self):
            self.obj.state = States.idle
            self.obj.speed = 0
            event.pub('ev_unit_state_switch', self.obj)
        def check(self):
            pass

        return (start, check)

    @fsm.state
    def step_state():
        def start(self):
            self.obj.state = States.moving
            self.obj.speed = self.obj.maxSpeed
            self.stime = None # start time
            event.pub('ev_unit_state_switch', self.obj)
        def check(self):
            if not self.stime:
                self.stime = timer.time

            # total time of travel A -> B [constant]
            tt = (cell_size / self.obj.speed) * 4
            srcPos = Vector3(self.obj.x * cell_size, self.obj.pos.y, self.obj.y * cell_size)
            dstPos = self.obj.dstPos

            step = (timer.time - self.stime) / tt
            nextPos = Vector3(geom.lerp(srcPos.x, dstPos.x, step), srcPos.y, geom.lerp(srcPos.z, dstPos.z, step))
            if round(step, 2) < 1.0:
                self.obj.pos = nextPos
            else:
                self.obj.pos = self.obj.dstPos
                self.obj.dstPos = None
                self.obj.x = self.obj.pos.x // cell_size
                self.obj.y = self.obj.pos.z // cell_size
                return "idle"

        return (start, check)


class UnitAI(fsm.StateMachine):

    def __init__(self, obj):
        self.obj = obj
        self.state = 'idle'
        self.dt = None
        self.pursuit_obj = None
        fsm.StateMachine.__init__(self)

    @fsm.state
    def idle_state():
        def start(self):
            self.state = 'idle'
            self.pursuit_obj = None
        def check(self):
            pass

        return (start, check)

    # Ai
    @fsm.state
    def pursuit_state():
        def start(self):
            self.state = 'pursuit'

            # move close to target
            o = self.pursuit_obj
            if not geom.in_radius(o.x, o.y, self.obj.fireRange, self.obj.x, self.obj.y):
                self.obj.moveTo(*geom.point_on_radius(self.obj.x, self.obj.y, o.x, o.y, self.obj.fireRange))
            else:
                pass # todo: add min fire range

        def check(self):
            if geom.in_radius(self.pursuit_obj.x, self.pursuit_obj.y, self.obj.fireRange, self.obj.x, self.obj.y):
                return "idle"

        return (start, check)



class Unit(Entity):
  typename = 'default'
  states_t = States
  maxSpeed = 20 # km/h
  def __init__(self, x=0, y=0):
      Entity.__init__(self)
      self.name = util.get_autoname(self.typename)
      self.pos = None # vector3, field's meters
      self.dstPos = None # 2d
      self.dir = 0
      self.x = x # field's cell x
      self.y = y # field's cell y
      self.path = []
      self.route = None
      self.target = None
      self.state = States.idle
      self.speed = 0
      self.damageLevel = 0
      self.ev_onSetDir = False # fixme
      self.fsm = UnitFSM(self)
      self.ai = UnitAI(self)

  def spawn(self):
      self.pos = Vector3(self.x * cell_size, 0, self.y * cell_size)
      event.pub('ev_unit_spawn', self)

  def setDir(self, angle):
      print '::setDir', angle
      self.dir = angle
      self.ev_onSetDir = True

  def moveTo(self, x, y):
      if x < 0 or y < y:
          print "warn: moveTo negative value, ignored.", x, y
          return []

      if self.state == States.idle:
          self.path = pathfind.path((self.x, self.y), (x, y))
      else:
          # in transition, move from next node
          nextdst = (self.path[-1].x, self.path[-1].y)
          self.path = pathfind.path(nextdst, (x, y))
      print self.path

  def isSelected(self):
      return True if self in Game().unitmgr.selectedUnits else False

  def isDestroyed(self):
      return True if self.damageLevel >= self.maxDamage else False

  def isArmed(self):
      return True if hasattr(self, 'fireRange') else False # fixme

  def bindRoute(self, route):
      self.route = route

  def setTarget(self, ob):
      self.target = ob

  def canAttack(self, ob):
      return True if self.isArmed() else False

  def takeDamage(self, amg):
      self.damageLevel += amg
      #event.pub_delayed(1, 'ev_unit_hit', self)
      event.pub('ev_unit_hit', self)

  def attack(self, target):
      self.setTarget(target)
      self.ai.pursuit_obj = target
      self.ai.set_state('pursuit')

  def _step(self, x, y):
      print '::step to', x, y
      deg = geom.deg_between(self.x, self.y, x, y)
      self.setDir(deg)
      self.dstPos = Vector3(x * cell_size, self.pos.y, y * cell_size)
      self.fsm.set_state("step")
      event.pub('ev_unit_step', self)

  def _handle_path(self):
      n = self.path[-1]
      if n.x == self.x and n.y == n.y: # node reached
          self.path.pop()
          if not self.path:
              # it was last node
              return
          n = self.path[-1]
      print 'handled', n
      self._step(n.x, n.y)
  
  def _handle_route(self):
      if self.path:
          print "err"
          exit()

      self.path = self.route.getNextPath((self.x, self.y))

  def _handle_target(self):

      if self.target.isDestroyed():
          # unset target
          event.pub('ev_unit_destroy', self.target)
          self.setTarget(None)
          # destroy target
          return

      self._handle_aiming()

      if self.canFire() and \
         geom.in_radius(self.target.x, self.target.y, self.fireRange, self.x, self.y):
          self.fire()

      # waiting for hit
      if self.waitForHitTime:
          self.waitForHit()

  def update(self, dt):

      self.fsm.dt = dt
      self.fsm.update()

      self.ai.dt = dt
      self.ai.update()

      # process state
      if self.state == States.idle:
          if self.path:
              self._handle_path()
          elif self.route:
              self._handle_route()

      if self.target:
          self._handle_target()

