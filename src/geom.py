from __future__ import division, absolute_import
from math import atan2, sin, cos, degrees, radians, sqrt

def deg_between(x1, y1, x2, y2):
    return degrees(atan2(y2 - y1, x2 - x1))

def in_rect(x, y, rx, ry, rw, rh):
    return True if x >= rx and x <= rx + rw and y >= ry and y <= ry + rh else False

def in_radius(x, y, r, cx=0, cy=0):
    """
    cx, cy - center of circle
    x, y - point
    r - radius
    """
    x -= cx
    y -= cy
    return True if x**2 + y**2 <= r**2 else False


def point_on_radius(x, y, cx, cy, r):
    """
    x, y - (A) unit
    cx, cy - (B) target
    r - radius
    """

    # angle from B to A
    a = deg_between(cx, cy, x, y)
    ar = radians(a)

    print 'a:', a
    
    # point on circle
    px, py = cx + r * cos(ar), \
             cy + r * sin(ar)

    print px, py

    return int(px), int(py)

def lerp(a, b, t):
    return a+(b-a)*t

def vec2add(a, b):
    return tuple(map(lambda x,y: x+y, a, b))

def dist(t1, t2):
    return sqrt((t2[0] - t1[0])**2 + (t2[1] - t1[1])**2)

