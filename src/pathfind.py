from __future__ import division, absolute_import
from src.game import Game

field = Game().field
gcost_straight = 10 # straight unit move cost
gcost_diagonal = 14 # diagonal unit move cost
fh = 128
fw = 128

class Node(object):
    def __init__(self, x, y):
        self.x, self.y = x, y
        self.fs = 0
        self.parent = None
    def __cmp__(self, o):
        return 0 if self.x == o.x and self.y == o.y else 1 # fixme
    def __repr__(self):
        return '<Node %d, %d, fs=%d>' % (self.x, self.y, self.fs)
    
def neighbours(x, y):
    r = ([x,y+1], [x+1,y+1], [x+1,y],
         [x+1,y-1], [x,y-1], [x-1,y-1],
         [x-1,y], [x-1,y+1])
    # filter negative
    r = [t for t in r if t[0] >= 0 and t[1] >= 0]
    return r

def get_fscore_func(start, goal): # fixme
    def fscore(a, b):
        return abs(a.x - b.x) + abs(a.y - b.y) # manhattan
    return fscore

def get_lowest_fscore_node(l): #fixme: use sorting
    ret = l[0]
    for x in l:
        ret = x if x.fs < ret.fs else ret
    return ret

def traceback_path(n):
    while n.parent:
        yield n
        n = n.parent
    yield n
    
def path(start_, goal_, **kw):
    if not field.isCellEmpty(*goal_):
        print "No path to closed cell"
        return False
    start = Node(*start_)
    goal = Node(*goal_)
    cset = [] # closed set
    oset = [start] # open set
    path = []
    fscore = get_fscore_func(start, goal)
    print 'FROM', start, 'TO', goal
    while len(oset) > 0:
        #print 'iter'
        x = get_lowest_fscore_node(oset)
        if x == goal:
            print 'GOAL'
            path = list(traceback_path(x))
            return path

        oset.remove(x)
        cset.append(x)
        for y in [Node(*pos) for pos in neighbours(x.x, x.y)]: # get a slice
            if y in cset: # fixme
                continue # already done

            # check empty
            if not field.isCellEmpty(y.x, y.y):
                #cset.append(y) ??
                continue # skip

            #if check_z:
            #    #lev = game.field.zlevel[y.x][y.y]
            #    lev = field.getCellHeight([y.x], [y.y])
            #    if not (lev >= zmin and lev <= zmax):
            #        continue # `closed`
            y.parent = x
            y.fs += fscore(y, goal)
            if y not in oset:
                oset.append(y) # process node in next iter
    print "No path found"
    return False


def truncate_path(srcpath, new_dst):
    # truncate path to a new branch
    raise NotImplementedError
    print path
    lastn = srcpath[-1]
    newpath = path((lastn.x, lastn.y), new_dst)
    return newpath
    
if __name__ == '__main__':
    _path = path((1, 1), (5, 5))
    #_path.pop()
    #_path = truncate_path(_path, (5, 5))
    print _path
