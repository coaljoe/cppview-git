from OpenGL.GL import *
from src.lib import glfw
import PyCEGUI
import PyCEGUIOpenGLRenderer
CEGUI_PATH = "data/gui/"

def hide_parent_window_cb(e):
    e.window.getParent().setVisible(False)
    return true;

class Gui(object):
    def __init__(self, app):
        self.app = app
        PyCEGUIOpenGLRenderer.OpenGLRenderer.bootstrapSystem()
        self.setup()

    def setup(self):
        # resources
        rp = PyCEGUI.System.getSingleton().getResourceProvider()
 
        rp.setResourceGroupDirectory("schemes", CEGUI_PATH + "schemes")
        rp.setResourceGroupDirectory("imagesets", CEGUI_PATH + "imagesets")
        rp.setResourceGroupDirectory("fonts", CEGUI_PATH + "fonts")
        rp.setResourceGroupDirectory("layouts", CEGUI_PATH + "layouts")
        rp.setResourceGroupDirectory("looknfeels", CEGUI_PATH + "looknfeel")
        rp.setResourceGroupDirectory("schemas", CEGUI_PATH + "xml_schemas")
 
        PyCEGUI.Imageset.setDefaultResourceGroup("imagesets")
        PyCEGUI.Font.setDefaultResourceGroup("fonts")
        PyCEGUI.Scheme.setDefaultResourceGroup("schemes")
        PyCEGUI.WidgetLookManager.setDefaultResourceGroup("looknfeels")
        PyCEGUI.WindowManager.setDefaultResourceGroup("layouts")
 
        parser = PyCEGUI.System.getSingleton().getXMLParser()
        if parser.isPropertyPresent("SchemaDefaultResourceGroup"):
            parser.setProperty("SchemaDefaultResourceGroup", "schemas")     
 
        # gui
        PyCEGUI.SchemeManager.getSingleton().create("WindowsLook.scheme")
        PyCEGUI.FontManager.getSingleton().create("gui.font");
        PyCEGUI.System.getSingleton().setDefaultFont("gui_font")
        PyCEGUI.System.getSingleton().setDefaultMouseCursor("WindowsLook", "MouseArrow")

        wmgr = PyCEGUI.WindowManager.getSingleton()
        self.root = wmgr.createWindow("DefaultWindow", "root")
 
        unitprod = PyCEGUI.WindowManager.getSingleton().loadWindowLayout("unitprod.layout")
        win = unitprod.getChildRecursive("CloseButton")
        win.subscribeEvent(PyCEGUI.PushButton.EventClicked, hide_parent_window_cb, "");
        unitprod.setVisible(False)
        self.root.addChildWindow(unitprod)
 
        PyCEGUI.System.getSingleton().setGUISheet(self.root)

        # resize cb
        PyCEGUI.System.getSingleton().notifyDisplaySizeChanged(PyCEGUI.Size(self.app.w._width, self.app.w._height))

    def __del__(self):
        PyCEGUIOpenGLRenderer.OpenGLRenderer.destroySystem()

    def draw(self, dt):
        PyCEGUI.System.getSingleton().injectTimePulse(dt)
 
        # fix
        #glPopAttrib()

        glMatrixMode(GL_PROJECTION);
        #glBindBuffer(GL_ARRAY_BUFFER,0);
        #glActiveTexture(0)
        glOrtho(0,800,0,600,-100,100)
        glMatrixMode(GL_MODELVIEW)
        #h3dClearOverlays();

        PyCEGUI.System.getSingleton().renderGUI()

        # fix
        #glPushAttrib(GL_ALL_ATTRIB_BITS)

    def on_mouse_button(self, button, pressed):
        if button == glfw.MOUSE_BUTTON_LEFT:
            if not pressed:
                PyCEGUI.System.getSingleton().injectMouseButtonUp(PyCEGUI.LeftButton)
            else:
                PyCEGUI.System.getSingleton().injectMouseButtonDown(PyCEGUI.LeftButton)
        elif button == glfw.MOUSE_BUTTON_RIGHT:
            if not pressed:
                PyCEGUI.System.getSingleton().injectMouseButtonUp(PyCEGUI.RightButton)
            else:
                PyCEGUI.System.getSingleton().injectMouseButtonDown(PyCEGUI.RightButton)
 

    def on_mouse_pos(self, x, y, dx, dy):
        PyCEGUI.System.getSingleton().injectMousePosition(x, y)

