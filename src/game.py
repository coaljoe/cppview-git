from __future__ import division, absolute_import
from time import time
from src.util import Singleton
from src.field import Field
from src.unitmgr import UnitManager
from src.player import Player, PlayerManager
from src.hud import HUD
from src.view.viewmgr import ViewMgr

class Timer(object):
    __metaclass__ = Singleton
    def __init__(self):
        self.speed = 1.0

    def start(self):
        self.start_time = time()
        self.time = self.start_time

    def update(self, dt):
        self.time = (time() - self.start_time) * self.speed

timer = Timer()

class Game(object):
    __metaclass__ = Singleton
    def __init__(self):
        self.frame = 0
        self.timer = timer
        self.timer.speed = 1.0
        self.timer.start()
        self.objs = []
        self.unitmgr = UnitManager(self)
        self.playermgr = PlayerManager()
        self.playermgr.addPlayer(Player('Default', camp='suur')) # current player
        self.field = Field()
        self.hud = HUD(self)
        from src.prodman import ProductionManager
        self.prodman = ProductionManager(self)
        from src import event as _event # fixme
        self.event = _event.EventManager(self)
        self.viewmgr = ViewMgr(self)
        self.shed_gen = None

    def update(self, dt):
        dt = dt * self.timer.speed
        self.timer.update(dt)

        for x in self.objs:
            x.update(dt)

        self.unitmgr.update(dt)
        self.prodman.update(dt)
        self.viewmgr.update(dt)

        try: self.shed_gen.next()
        except: pass

        self.frame += 1
    
    def step(self):
        pass

    def getGameState(self):
        return {'objs': self.objs}

    def setGameState(self, gs):
        self.objs = gs['objs']

    @property
    def player(self):
        return self.playermgr.player

    @property
    def time(self):
        return self.timer.time

