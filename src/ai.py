from __future__ import division, absolute_import
from src.player import Player
from src.units.tank import Tank
from src.building import Building
from src.buildings.factory import Factory

class AIPlayer(Player):
    def __init__(self, name, **kw):
        Player.__init__(self, name, 'AI', **kw)

    def spawn(self, hqpos):
        self.factory = Factory(*hqpos)
        self.factory.spawn()
        b = Building(hqpos[0] + 5, hqpos[1] - 2)
        b.spawn()

    def buildUnit(self):
        u1 = Tank()
        self.factory.produce(u1)

    def update(self):
        if self.money > 100:
            self.buildUnit()
