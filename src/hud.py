from __future__ import division, absolute_import
from OpenGL.GL import *
from src.lib import glfw

class States:
    idle = 0
    selection = 1

class HUD(object):
    def __init__(self, game):
        self.selObjects = []
        self.selUnits = []
        self.selBuilding = None
        self.game = game
        self.state = 'idle'

        self.selrect = [0, 0, 0, 0]
        self.width = 800
        self.height = 600

    def unselectAll(self):
        for u in self.selUnits:
            self.game.event.pub('ev_hud_remove_selection', u)

        self.selUnits = []
        self.selBuilding = None

        #event.pub('ev_hud_unselect_all', obj)

    def onUnitSelect(self, u): # onObjectSelect?
        if u.player != self.game.player:
            return

        if u not in self.selUnits:
            self.selUnits.append(u)
            self.game.event.pub('ev_hud_add_selection', u)
        else:
            print 'already selected', u

    def onUnitUnselect(self, u):
        self.selUnits.remove(u)
        self.game.event.pub('ev_hud_remove_selection', u)

    def onObjectClick(self, object):
        if object.player != self.game.player:
            if self.selUnits:
                for u in self.selUnits:
                    if u.canAttack(object):
                        u.attack(object)
            return

        from src.building import Building
        if isinstance(object, Building):
            self.selBuilding = object
            self.game.event.pub('ev_gui_show_window', 'UnitProd')
            print 'selected building', self.selBuilding
        else:
            unit = object
            print 'onObjectClick', unit, self.state
            #if self.state == States.idle:
            #    self.onUnitSelect(unit)
            self.onUnitSelect(None, unit)


    def onHudBeginSelection(self):
        print 'onHudBeginSelection'
        self.unselectAll()

    def onHudEndSelection(self):
        print 'LOL', self.selUnits

    def onObjectMouseOver(self, sender, obj):
        print "onObjectMouseOver", sender, obj

        if obj.player != self.game.player:
            for u in self.selUnits:
                if u.canAttack(obj):
                    self.game.event.pub('ev_gui_set_cursor', 'AttackCursor')
                    break

    def onFieldClick(self, sender):
        print 'LOL2', self.selUnits
        for x in self.selUnits:
            x.moveTo(self.game.field.pointer_x, self.game.field.pointer_y)

    def onUnselectAll(self):
        self.unselectAll()

    def on_mouse_pos(self, x, y, dx, dy):
        self.mx = x
        self.my = y

        # update selecteion
        if self.state == 'selection':
            self.selrect[2] = x
            self.selrect[3] = y

    def on_mouse_button(self, button, pressed):
        if button != glfw.MOUSE_BUTTON_LEFT:
            self.unselectAll()

        if self.state == 'idle':
            if pressed:
                self.selrect[0] = self.mx
                self.selrect[1] = self.my
                self.state = 'selection'
        elif self.state == 'selection':
            if not pressed:
                self.state = 'idle'

    def draw(self, dt):
        # fix
        #glPopAttrib()

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity()

        #glBindBuffer(GL_ARRAY_BUFFER,0);
        #glActiveTexture(0)
        glOrtho(0,self.width,0,self.height,-1,1)
        glMatrixMode(GL_MODELVIEW)

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
        glColor3f(1., 1., 1.)
        #glRectf(-0.75,0.75, 0.75, -0.75)
        #glRectf(100,200, 200, 100)
        #glRectf(*self.selrect)
        if self.state == 'selection':
            z = self.selrect
            glRectf(z[0], self.height - z[1], z[2], self.height - z[3])
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

        # fix
        #glPushAttrib(GL_ALL_ATTRIB_BITS)
