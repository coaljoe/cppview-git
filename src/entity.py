from __future__ import division, absolute_import
from itertools import count
from src.game import Game

class Entity(object):
    """Game object"""
    entity_counter = count()

    def __init__(self):
        self.player = Game().player
        self.entity_id = self.entity_counter.next()
        self.name = None

    @property
    def xy(self):
        return self.x, self.y

    @xy.setter
    def xy(self, val):
        self.x, self.y = val

