from __future__ import division, absolute_import
from src.building import Building

class PowerPlant(Building):
    typename = 'power_plant'
    def __init__(self, *args, **kw):
        Building.__init__(self, *args, **kw)

        # type's props
        self.place_size = (2, 2)
        self.cargo_point = (-1, -1)


