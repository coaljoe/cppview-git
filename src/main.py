#!/usr/bin/env python
from __future__ import division, absolute_import
import sys
sys.path.insert(0, '..')
sys.dont_write_bytecode = True
from math import *
from OpenGL.GL import *
from src.lib import glfw
from src.lib import h3d
from src.game import Game
from src.gui import Gui
from src.units.tank import Tank
from src.units.lighttank import LightTank
from src.units.dumptruck import DumpTruck
from src.buildings.powerplant import PowerPlant
from src.buildings.factory import Factory
from src.route import CargoRoute
#from src.player import Player
#from src.ai import AIPlayer
#import rpdb2
#rpdb2.start_embedded_debugger("1")

from src.view import app

class Window(app.Window):
    def on_key_press(self, key):
        if key == glfw.KEY_ESC:
            self.has_exit = True
        #elif key == glfw.KEY_SPACE:
        #    self._app._freeze = not self._app._freeze
        elif key == glfw.KEY_F7:
            self._app._debugViewMode = not self._app._debugViewMode
        elif key == glfw.KEY_F8:
            self._app._wireframeMode = not self._app._wireframeMode
        elif key == glfw.KEY_F9:
            self._app._showStats = True
            self._app._statMode = 1 - self._app._statMode

        #self._app.gui.on_key_press(key)

    def on_mouse_pos(self, x, y, dx, dy):
        #print 'lel', dx, dy, x,y
        # Look left/right
        self._app._ry -= dx / 100.0 * 30.0
        # Loop up/down but only in a limited range
        self._app._rx = max(-90, min(90, self._app._rx + dy / 100.0 * 30.0))

        self._app.gui.on_mouse_pos(x, y, dx, dy)
        Game().hud.on_mouse_pos(x, y, dx, dy)

    def on_mouse_button(self, button, pressed):
        self._app.gui.on_mouse_button(button, pressed)
        Game().hud.on_mouse_button(button, pressed)

class XApp(app.App):
    def __init__(self, windowCls):
        app.App.__init__(self, windowCls)
        self.gui = Gui(self)

        self._x = 0.0
        self._y = 100.0
        self._z = 0.0
        self._rx = -90
        self._ry = 270.0
        self._rz = 0
        self._velocity = 50.0
        
        self._freeze = False
        self._showStats = False
        self._debugViewMode = False
        self._wireframeMode = False

        self._statMode = 0

    def _h3dAddResources(self):
        h3dres = self._h3dres

        # Pipelines
        #h3dres.hdrPipe = h3d.addResource(h3d.ResTypes.Pipeline, "pipelines/hdr.pipeline.xml", 0)
        h3dres.forwardPipe = h3d.addResource(h3d.ResTypes.Pipeline, "pipelines/forward.pipeline.xml", 0)
        # Overlays
        h3dres.fontMat = h3d.addResource(h3d.ResTypes.Material, "overlays/font.material.xml", 0)
        h3dres.panelMat = h3d.addResource(h3d.ResTypes.Material, "overlays/panel.material.xml", 0)
        h3dres.logoMat = h3d.addResource(h3d.ResTypes.Material, "overlays/logo.material.xml", 0)
        # Environment
        h3dres.env = h3d.addResource(h3d.ResTypes.SceneGraph, "models/sphere/sphere.scene.xml", 0)
        # Knight
        h3dres.knight = h3d.addResource(h3d.ResTypes.SceneGraph, "models/knight/knight.scene.xml", 0)
        h3dres.man = h3d.addResource(h3d.ResTypes.SceneGraph, "models/man/man.scene.xml", 0)
        # Terrain
        h3dres.terrain = h3d.addResource(h3d.ResTypes.SceneGraph, "terrains/terrain1/terrain1.scene.xml", 0)
        h3dres.matRes = h3d.findResource(h3d.ResTypes.Material, "terrains/terrain1/terrain1.material.xml" );

        h3d.utils.loadResourcesFromDisk('Content')

    def _h3dSetupScene(self):
        h3dres = self._h3dres

        #self._env = h3d.addNodes(h3d.RootNode, h3dres.env)
        #h3d.setNodeTransform(self._env, 0, -20, 0, 0, 0, 0, 20, 20, 20)

        #self._knight = h3d.addNodes(h3d.RootNode, h3dres.knight)
        #h3d.setNodeTransform(self._knight, 0, 0, 0, 0, 180, 0, .1, .1, .1)

        # Add terrain
        self.terrain = h3d.addNodes(h3d.RootNode, h3dres.terrain)
        # Set sun direction for ambient pass
        h3d.setMaterialUniform(h3dres.matRes, "sunDir", 1, -1, 0, 0 );
        #MeshQualityF = 10002
        #SkirtHeightF = 10003
        h3d.setNodeParamF(self.terrain, 10002, 0, 100)
        h3d.setNodeParamF(self.terrain, 10003, 0, 0.1)

        self._light = h3d.addLightNode(h3d.RootNode, 'Light1', 0, 'LIGHTING', 'SHADOWMAP')
        h3d.setNodeTransform(self._light, 30, 50, 30, -60, 0, 0, 1, 1, 1)
        h3d.setNodeParamF(self._light, h3d.Light.RadiusF, 0, 300)
        h3d.setNodeParamF(self._light, h3d.Light.FovF, 0, 90)
        h3d.setNodeParamI(self._light, h3d.Light.ShadowMapCountI, 4)
        h3d.setNodeParamF(self._light, h3d.Light.ShadowMapBiasF, 0, 0.01)
        h3d.setNodeParamF(self._light, h3d.Light.ColorF3, 0, .5)
        h3d.setNodeParamF(self._light, h3d.Light.ColorF3, 1, .5)
        h3d.setNodeParamF(self._light, h3d.Light.ColorF3, 2, .5)

    def _mainloopUpdate(self, dt):
        app.App._mainloopUpdate(self, dt)

        w = self.w
        h3d.setNodeTransform(w.camera, self._x, self._y, self._z, self._rx, self._ry, self._rz, 1, 1, 1)
        #print self._rx, self._ry, self._x, self._y, self._z

        if self._debugViewMode:
            h3d.setOption(h3d.Options.DebugViewMode, 1.0)
        else:
            h3d.setOption(h3d.Options.DebugViewMode, 0.0)

        if self._wireframeMode:
            h3d.setOption(h3d.Options.WireframeMode, 1.0)
        else:
            h3d.setOption(h3d.Options.WireframeMode, 0.0)

        curVel = self._velocity * dt
        if self.w.isPressed(glfw.KEY_LSHIFT):
            curVel *= 5
        if self.w.isPressed('w'):
            self._x -= sin(radians(self._ry)) * cos(-radians(self._rx)) * curVel
            self._y -= sin(-radians(self._rx)) * curVel
            self._z -= cos(radians(self._ry)) * cos(-radians(self._rx)) * curVel
        elif self.w.isPressed('s'):
            self._x += sin(radians(self._ry)) * cos(-radians(self._rx)) * curVel
            self._y += sin(-radians(self._rx)) * curVel
            self._z += cos(radians(self._ry)) * cos(-radians(self._rx)) * curVel
        if self.w.isPressed('a'):
            self._x += sin(radians(self._ry - 90)) * curVel
            self._z += cos(radians(self._ry - 90)) * curVel
        elif self.w.isPressed('d'):
            self._x += sin(radians(self._ry + 90)) * curVel
            self._z += cos(radians(self._ry + 90)) * curVel

xapp = XApp(Window)
xapp.init()

game = Game()

#p2 = AIPlayer('Ai', camp='reds')
#game.playermgr.addPlayer(p2)
#game.playermgr.swapPlayer(p2)
#p2.spawn((20, 48))
#p2.update()
#game.playermgr.swapPlayer()

t1 = LightTank(1, 1)
t1.spawn()
t1.setDir(90)
#t1.moveTo(3, 5)

#game.playermgr.swapPlayer(p2)

t2 = Tank(0, 0)
t2.spawn()
#assert t2.dir.valueDegrees() == 0
#t2.moveTo(2, 1) 

#t1.setTarget(t2)
#t1.attack(t2)

#game.playermgr.swapPlayer()
#assert t1.player != t2.player

game.objs.append(t1)
game.objs.append(t2)

u3 = DumpTruck(3, 1)
u3.spawn()
#u3.setSelected()
u3.setDir(0)

#u4 = DumpTruck(4, 9)
#u4.spawn()
#u4.setSelected()

#t1.setTarget(u4)


#u4.takeDamage(1000)
#game.event.ev_unit_destroy(u3, obj=u3)
#u3.destroy()

u3.unloadCargoState()
#u3.moveTo(2, 6)

#game.objs.append(t1)
#game.objs.append(t2)
game.objs.append(u3)
#game.objs.append(u4)


#from src import pathfind
#path = pathfind.path((2, 2), (3, 6))
#print path

b1 = PowerPlant(5, 10)
b1.spawn()

b2 = Factory(5, 20)
b2.spawn()

#game.prodman.buyUnit('tank', b2.name)

#field.field.saveLandmap()

#from src import pathfind
#path = pathfind.path((1, 1), (2, 5))
#print path

#CargoMgr.addRoute('route1', b1, b2)
#CargoMgr.bindToRoute('route1', u3)

route1 = CargoRoute()
route1.addTargets(b1, b2)
print route1.path

#route1.bindUnit(u4)
#u4.bindRoute(route1)


#for i in xrange(1,10):
#    x = Tank(2, i)
#    x.spawn()
#    x1 = Tank(3, i)
#    x1.spawn()

while xapp.running:
    dt = xapp._dt
    game.update(dt)
    xapp.step()
    xapp.begin_custom_gl()
    game.hud.draw(dt)
    xapp.gui.draw(dt)
    xapp.end_custom_gl()
    xapp.flip()
