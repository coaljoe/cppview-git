from __future__ import division, absolute_import
import message

class EventManager(object):
    """Game's event subsystem
    """
    def __init__(self, game):
        self.game = game

        # events list
        self.sub('ev_unit_step', self.game.unitmgr.onUnitStep)
        self.sub('ev_field_cell_activate', self.game.unitmgr.onFieldCellActivate)
        self.sub('ev_object_mouse_over', self.game.hud.onObjectMouseOver)
        #self.sub('ev_no_object_mouse_over', Game().unitmgr.onNoObjectMouseOver)
        #self.sub('ev_field_click', self.game.hud.onFieldClick)
        self.sub('ev_field_click', self.game.hud.onFieldClick)
        self.sub('ev_object_click', self.game.hud.onObjectClick)
        self.sub('ev_hud_unit_select', self.game.hud.onUnitSelect)
        self.sub('ev_hud_unit_unselect', self.game.hud.onUnitUnselect)
        self.sub('ev_hud_unselect_all', self.game.hud.onUnselectAll)
        self.sub('ev_unit_spawn', self.game.unitmgr.onUnitSpawn)
        self.sub('ev_unit_destroy', self.game.unitmgr.onUnitDestroy)
        self.sub('ev_building_spawn', self.empty_handler)

        self.sub('ev_hud_begin_selection', self.game.hud.onHudBeginSelection)
        self.sub('ev_hud_end_selection', self.game.hud.onHudEndSelection)
        self.sub('ev_gui_place_building', self.ev_gui_place_building_handler)

    def empty_handler(self, *args, **kw):
        pass

    def sub(self, evname, handler):
        message.sub(evname, handler)

    def pub(self, evname, *args, **kw):
        message.pub(evname, *args, **kw)

    def pub_delayed(self, delay, evname, *args, **kw):
        def _do(self, delay, evname, *args, **kw):
            exec_time = self.game.time + delay
            while self.game.time < exec_time:
                yield

            self.pub(evname, *args, **kw)

        self.game.shed_gen = _do(self, delay, evname, *args, **kw)

    # XXX move to buildingMgr
    def ev_gui_place_building_handler(self, **kw):

        field = self.game.field
        from src.building import Building
        from src.buildings.powerplant import PowerPlant
        
        if kw['building_typename'] == 'power_plant':
            b = PowerPlant(field.pointer_x, field.pointer_y)
            b.spawn()
        elif kw['building_typename'] == 'default':
            b = Building(field.pointer_x, field.pointer_y)
            b.spawn()
        else:
            print "Unknown building"

