#!/bin/sh
cd cppview
sudo cpufreq-set -u 2.7ghz
#ulimit -m 200000
nice scons -j2 app && cp cppview ../cppview.bin
ret=$?
cd ..
sudo cpufreq-set -u 1.5ghz

exit $ret
